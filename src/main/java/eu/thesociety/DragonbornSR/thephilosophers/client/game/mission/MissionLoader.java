package eu.thesociety.DragonbornSR.thephilosophers.client.game.mission;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.igormaznitsa.prol.io.DefaultProlStreamManagerImpl;
import com.igormaznitsa.prol.logic.ProlContext;
import com.igormaznitsa.prol.parser.ProlConsult;

import eu.thesociety.DragonbornSR.thephilosophers.client.utils.FileUtil;

public class MissionLoader {
	
	private static ArrayList<Mission> availableMissions = new ArrayList<Mission>();
	
	static enum MissionApiVersion {
		V_1("mission-api.pl");
		
		private final String file;
		
		MissionApiVersion(String file) {
			this.file = file;
		}

		public String getApiFileName() {
			return file;
		}
	}
	
	public static Mission loadMission(String missionLocation, boolean isSteamWorkshopContent) {
		String missionApiStr = FileUtil.fileToString("missions/mission-api.pl");
		String missionStr = FileUtil.fileToString(missionLocation + "/mission.pl");
		
		ProlContext context = null;
		ProlConsult consult = null;
		try {
			context = new ProlContext(missionLocation,DefaultProlStreamManagerImpl.getInstance());
			consult = new ProlConsult(missionApiStr + missionStr, context);
			consult.consult();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new Mission(missionLocation, context, isSteamWorkshopContent);
	}
	
	public static ArrayList<Mission> getAvailableMissions() {
		return availableMissions;
	}
	
	public static void refreshAvailableMissions() {
		availableMissions.clear();
		//premade missions
		ArrayList<String> foldernames = getFolders("missions");
		for (String foldername : foldernames) {
			availableMissions.add(loadMission("missions/" + foldername, false));
		}
		// TODO ** WORKSHOP MISSIONS ** //
	}
	
	public static ArrayList<String> getFolders(String sDir) {
		ArrayList<String> foldernames = new ArrayList<String>();
		File[] faFiles = new File(sDir).listFiles();
		for (File file : faFiles) {
			if (file.isDirectory()) {
				foldernames.add(file.getName());
			}
		}
		return foldernames;
	}

}
