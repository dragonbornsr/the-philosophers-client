package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import java.awt.Color;

import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GameLogicGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TextButton;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Matchmaking;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.components.GuiComponentPlayerlist;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListLobbyChat;

public class MPLobbyGui extends GameLogicGui {
	
	private TextButton quitRoomButton;
	private ScrollableListLobbyChat chatBox;
	private GuiTextBox chatInputBox;
	private GuiComponentPlayerlist playerlist;
	
	public MPLobbyGui() {
		this.setSize(Display.WIDTH, Display.HEIGHT);
		Texture t = new Texture("resources/white.png");
		this.setBackgroundTransparency(0.0f);
		this.setBackground(t);
		this.setPosition(0, 0);
		this.register(PRIORITY_MEDIUM);
		this.setVisible(false);
		this.setEnabled(false);	
		
		this.quitRoomButton = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("resources/missing.png"))
				.setSizeAndPosition(this.width - 250, 150, 150, 40, 0, 70));
		
		this.quitRoomButton.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(36)
				.setText("Leave Room");
				
		this.quitRoomButton.setVisible(true);

		setChatBox(new ScrollableListLobbyChat(this));
		
		this.chatInputBox = new GuiTextBox(this)
				.setBackground(ResourceManager.getTexture("resources/missing.png"))
				.setPositionAndSize(100, 150, chatBox.getWidth(), 30)
				.setDefaultString("message");
		
	}
	
	@Override
	public void resume() {
		this.setVisible(true);
		this.setEnabled(true);
		this.setRunning(true);
		this.playerlist = new GuiComponentPlayerlist(Display.WIDTH / 2 + 5, 200, Display.WIDTH / 2 - 100, Display.HEIGHT - 300, this, Matchmaking.getCurrentLobby());
	}

	@Override
	public void stop() {
		this.setVisible(false);
		this.setEnabled(false);
		this.setRunning(false);
		if (this.playerlist != null) {
			this.playerlist.unregister();
		}
	}

	@Override
	public void reset() {
		this.resume();
	}

	@Override
	public void buttonPressed(Button button) {
		
	}

	@Override
	public void buttonReleased(Button button) {
		if (button.equals(this.quitRoomButton)) {
			Matchmaking.instance().getSteamMatchmaking().leaveLobby(Matchmaking.getLobbyId());
			GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY_SELECTION);
		}
	}
	
	@Override
	public void textBoxChanged(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {
		if (textBox.equals(this.chatInputBox)) {
			Matchmaking.instance().getSteamMatchmaking().sendLobbyChatMsg(Matchmaking.getLobbyId(), textBox.getText());
			textBox.setText("");
		}
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.setSize(width, height);
		if (this.playerlist != null) {
			this.playerlist.setPosition(Display.WIDTH / 2 + 5, 200);
			this.playerlist.setSize(Display.WIDTH / 2 - 100, Display.HEIGHT - 300);
		}
	}

	/**
	 * @return the chatBox
	 */
	public ScrollableListLobbyChat getChatBox() {
		return chatBox;
	}

	/**
	 * @param chatBox the chatBox to set
	 */
	public void setChatBox(ScrollableListLobbyChat chatBox) {
		this.chatBox = chatBox;
	}

	public GuiTextBox getGuiInputBox() {
		// TODO Auto-generated method stub
		return this.chatInputBox;
	}
}
