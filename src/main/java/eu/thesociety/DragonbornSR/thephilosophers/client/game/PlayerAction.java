package eu.thesociety.DragonbornSR.thephilosophers.client.game;

/**
 * Action a certain player can execute. This will be built up
 * on information based on mission script action(...) predicate.
 * 
 * @author Oliver Tiit
 *
 */
public class PlayerAction {
	private Player player;
	private String group;
	private String parentGroup;
	private String actionName;
	private String[] targetList;
	
	private String actionText;
	private String actionImage;
	private String actionSound;
	private float actionDuration;
	
	public PlayerAction(Player player, String group, String parentGroup, String actionName, 
			String[] targetList, String actionText, String actionImage, String actionSound, float actionDuration) {
		setPlayer(player);
		setGroup(group);
		setParentGroup(parentGroup);
		setActionName(actionName);
		setTargetList(targetList);
		setActionText(actionText);
		setActionSound(actionSound);
		setActionDuration(actionDuration);
	}
	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}
	/**
	 * @param player the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}
	/**
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}
	/**
	 * @param group the group to set
	 */
	public void setGroup(String group) {
		this.group = group;
	}
	/**
	 * @return the parentGroup
	 */
	public String getParentGroup() {
		return parentGroup;
	}
	/**
	 * @param parentGroup the parentGroup to set
	 */
	public void setParentGroup(String parentGroup) {
		this.parentGroup = parentGroup;
	}
	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}
	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	/**
	 * @return the targetList
	 */
	public String[] getTargetList() {
		return targetList;
	}
	/**
	 * @param targetList the targetList to set
	 */
	public void setTargetList(String[] targetList) {
		this.targetList = targetList;
	}
	/**
	 * @return the actionText
	 */
	public String getActionText() {
		return actionText;
	}
	/**
	 * @param actionText the actionText to set
	 */
	public void setActionText(String actionText) {
		this.actionText = actionText;
	}
	/**
	 * @return the actionImage
	 */
	public String getActionImage() {
		return actionImage;
	}
	/**
	 * @param actionImage the actionImage to set
	 */
	public void setActionImage(String actionImage) {
		this.actionImage = actionImage;
	}
	/**
	 * @return the actionSound
	 */
	public String getActionSound() {
		return actionSound;
	}
	/**
	 * @param actionSound the actionSound to set
	 */
	public void setActionSound(String actionSound) {
		this.actionSound = actionSound;
	}
	/**
	 * @return the actionDuration
	 */
	public float getActionDuration() {
		return actionDuration;
	}
	/**
	 * @param actionDuration the actionDuration to set
	 */
	public void setActionDuration(float actionDuration) {
		this.actionDuration = actionDuration;
	}
	
}
