package eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.MPLobbyGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.ChatMessage;

public class ScrollableListLobbyChat extends ScrollableList {
	
	public static ScrollableListLobbyChat instance;

	public ScrollableListLobbyChat(MPLobbyGui parentGui) {
		super(parentGui, 100, 200, Display.WIDTH - 200,Display.HEIGHT - 300, new Texture("resources/white.png"));
		onDisplayResized(Display.WIDTH, Display.HEIGHT);
		instance = this;
	}

	@Override
	public void onDisplayResized(int width, int height) {
		super.onDisplayResized(width, height);
		this.width = width - 200;
		if (this.width > Display.WIDTH / 2 - 100) this.width = Display.WIDTH / 2 - 100;
		this.height = height - 300;
		if (!this.items.isEmpty()) {
			int error = this.height % this.items.get(0).getHeight();
			this.height -= error;
		}
		if ((GuiTextBox)((MPLobbyGui)this.parentGui).getGuiInputBox() != null)
			((GuiTextBox)((MPLobbyGui)this.parentGui).getGuiInputBox()).setWidth(this.width);
	}
	
	public void addMessage(ChatMessage chatMessage) {
		this.items.add(new ScrollableListItemLobbyChatEntry(this, chatMessage));
	}

	public void clear() {
		this.items.clear();
		
	}

}
