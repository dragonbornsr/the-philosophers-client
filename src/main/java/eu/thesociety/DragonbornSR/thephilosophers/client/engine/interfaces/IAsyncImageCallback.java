package eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces;

import java.awt.image.BufferedImage;

public interface IAsyncImageCallback {

	void onImageReceived(String url, BufferedImage bi);
	
}