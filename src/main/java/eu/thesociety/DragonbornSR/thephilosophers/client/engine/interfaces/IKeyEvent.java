package eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces;

public interface IKeyEvent {
	
	public void keyReleased(int key, int scancode, int mods);
	
	public void keyPressed(int key, int scancode, int mods);

	public void characterTyped(int codepoint);
}
