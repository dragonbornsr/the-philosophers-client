package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBTTAlignedQuad;
import org.lwjgl.stb.STBTTBakedChar;
import org.lwjgl.stb.STBTruetype;

import eu.thesociety.DragonbornSR.thephilosophers.client.utils.IOUtil;

import java.awt.Color;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Optional;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBTruetype.*;

public final class TrueTypeFont {

	private int fontHeight;
	private int lineHeight;

	int BITMAP_W;
	int BITMAP_H;

	int texID;

	FloatBuffer x;
	FloatBuffer y;
	STBTTBakedChar.Buffer cdata;
	STBTTAlignedQuad q;

	public TrueTypeFont(String filePath, int size) {
		this.setFontHeight(size);
		this.setLineHeight(size);

		BITMAP_W = 512;
		BITMAP_H = 512;

		texID = glGenTextures();
		cdata = STBTTBakedChar.mallocBuffer(96); // 96

		try {
			ByteBuffer ttf = IOUtil.ioResourceToByteBuffer(filePath, 160 * 1024);

			ByteBuffer bitmap = BufferUtils.createByteBuffer(BITMAP_W * BITMAP_H);
			stbtt_BakeFontBitmap(ttf, getFontHeight(), bitmap, BITMAP_W, BITMAP_H, 32, cdata);

			glBindTexture(GL_TEXTURE_2D, texID);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, BITMAP_W, BITMAP_H, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		glBindTexture(GL_TEXTURE_2D, 0);

		x = BufferUtils.createFloatBuffer(1);
		y = BufferUtils.createFloatBuffer(1);
		q = STBTTAlignedQuad.malloc();
	}

	public void draw(String text, int xPos, int yPos, Color color) {
		glPushMatrix();
		glColor4f((float)color.getRed() / 255f, (float)color.getGreen() / 255f, (float)color.getBlue() / 255f, (float)color.getAlpha() / 255f);
		glBindTexture(GL_TEXTURE_2D, texID);
		
		x.put(0, 0.0f);
		y.put(0, 0.0f);
		glBegin(GL_QUADS);
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (c == '\n') {
				y.put(0, y.get(0) + getFontHeight());
				x.put(0, 0.0f);
				continue;
			} else if (c < 32 || 128 <= c)
				continue;
			
			STBTruetype.stbtt_GetBakedQuad(cdata, BITMAP_W, BITMAP_H, c - 32, x, y, q, 1);
			
			glTexCoord2f(q.s0(), q.t0());
			glVertex2f(q.x0() + xPos, -q.y0() + yPos);
			
			glTexCoord2f(q.s1(), q.t0());
			glVertex2f(q.x1() + xPos, -q.y0() + yPos);
			
			glTexCoord2f(q.s1(), q.t1());
			glVertex2f(q.x1() + xPos, -q.y1() + yPos);

			glTexCoord2f(q.s0(), q.t1());
			glVertex2f(q.x0() + xPos, -q.y1() + yPos);


		}
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
		glColor4f(1f, 1f, 1f, 1f);
		glPopMatrix();
	}
	
	public int getTextWidth(String text) {
		ArrayList<Float> lines = new ArrayList<Float>();
		ArrayList<Float> linearr = new ArrayList<Float>();
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (c == '\n') {
				y.put(0, y.get(0) + getFontHeight());
				x.put(0, 0.0f);
				lines.add(linearr.get(linearr.size() - 1) - linearr.get(0));
				linearr.clear();
				continue;
			} else if (c < 32 || 128 <= c)
				continue;
			STBTruetype.stbtt_GetBakedQuad(cdata, BITMAP_W, BITMAP_H, c - 32, x, y, q, 1);
			if (linearr.isEmpty())
				linearr.add(Float.valueOf(q.x0()));
			else 
				linearr.add(Float.valueOf(q.x1()));
		}
		linearr.add(Float.valueOf(q.x1()));
		lines.add(linearr.get(linearr.size() - 1) - linearr.get(0));
		Optional<Float> result = lines.stream().max((x, y) -> x.compareTo(y));
		if (result.isPresent()) return result.get().intValue();
		return 0;
	}

	/**
	 * @return the lineHeight
	 */
	public int getLineHeight() {
		return lineHeight;
	}

	/**
	 * @param lineHeight
	 *            the lineHeight to set
	 */
	public void setLineHeight(int lineHeight) {
		this.lineHeight = lineHeight;
	}

	/**
	 * @return the fontHeight
	 */
	public int getFontHeight() {
		return fontHeight;
	}

	/**
	 * @param fontHeight
	 *            the fontHeight to set
	 */
	public void setFontHeight(int fontHeight) {
		this.fontHeight = fontHeight;
	}
}