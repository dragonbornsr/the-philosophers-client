package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import java.awt.Color;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.KeyboardController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.MouseController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IKeyEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Gui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;

public class DebugGui extends Gui implements IKeyEvent {
	
	private TrueTypeFont ttf;
	private TrueTypeFont ttfsmall;
	Color color = new Color(1f,1f,1f,0.8f);
	String info = "";
	int fontsize = 30;
	int framesToSkip = 10;
	int frame;
	public DebugGui() {
		this.register(PRIORITY_HIGHEST);
		this.setSize(300, 400);
		this.setPosition(0, Display.HEIGHT - this.height);
		this.setBackgroundTransparency(0.2f);
		this.setBackground(ResourceManager.getTexture("resources/white.png"));
		ttf = new TrueTypeFont("resources/fonts/DroidSans.ttf", fontsize);
		ttfsmall = new TrueTypeFont("resources/fonts/DroidSans.ttf", fontsize - 5);
	}

	@Override
	public void buttonPressed(Button button) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void buttonReleased(Button button) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.setPosition(0, height - this.height);
	}

	@Override
	public void keyReleased(int key, int scancode, int mods) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(int key, int scancode, int mods) {
		if (key == 290) this.setVisible(!this.isVisible());
	}

	@Override
	public void characterTyped(int codepoint) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textBoxChanged(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		if (frame % framesToSkip == 0) {
			info = "FPS: " +ThePhilosophersClient.th.getLastTPS();
			info += "\nn(DGO): " + ThePhilosophersClient.listOfDrawableGameObjects.size();
			info += "\nn(GO): " + ThePhilosophersClient.listOfGameObjects.size();
			info += "\nn(ML): " + MouseController.getListOfMouseEventListeners().size();
			info += "\nn(KL): " + KeyboardController.getlistOfKeyboardEventListeners().size();
			
			info += "\n\nUpdate: " +  String.format("%.02f", getPercentage(ThePhilosophersClient.th.getLastUpdateTime(), ThePhilosophersClient.th.getLastHandleTime())) + "%";
			info += "\nDraw: " + String.format("%.02f", getPercentage(ThePhilosophersClient.th.getLastDrawTime(), ThePhilosophersClient.th.getLastHandleTime())) + "%";
			info += "\nIdle: " + String.format("%.02f", getPercentage(ThePhilosophersClient.th.getLastIdleTime(), ThePhilosophersClient.th.getLastHandleTime())) + "%";
		}
		frame++;
	}

	@Override
	public void draw(float delta) {
		super.draw(delta);

		if (this.isVisible()) {
			ttf.draw(info, this.posX + 5, this.posY + this.height - fontsize, color);
			ttfsmall.draw("Debug Info. F1 to toggle", this.posX + 5, this.posY +5, color);
		}
	}
	
	public static float getPercentage(long n, long total) {
	    float proportion = ((float) n) / ((float) total);
	    return proportion * 100;
	}
}
