package eu.thesociety.DragonbornSR.thephilosophers.client.types;

import java.util.Collection;
import java.util.Vector;

public class Vector2f extends Vector<Object> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static int X = 0, Y = 1;
	
	public Vector2f(float x, float y) {
		super();
		this.setSize(2);
		this.add(X, x);
		this.add(Y, y);
	}
	
	public float getX() {
		return (float) this.get(X);
	}
	
	public float getY() {
		return (float) this.get(Y);
	}
	
	@Override
	public void setSize(int size) {
		super.setSize(2);
	}
	
	@Override
	public Object set(int i, Object o) {
		return super.set(i, o);
	}
	
	@Override
	public void setElementAt(Object o, int i) {
		super.setElementAt(o,i);
	}
	
	@Override
	public boolean add(Object o) {
		return super.add(o);
	}
	
	@Override
	public void addElement(Object o) {
		super.add(o);
	}
	
	@Override
	public void add(int i, Object o) {
		super.add(i, o);
	}
	
	@Override
	public boolean addAll(Collection<?> c) {
		return super.addAll((Collection<?>) c.stream().filter(x -> x instanceof Float));
	}
	
	@Override
	public boolean addAll(int i, Collection<?> c) {
		return super.addAll(i, (Collection<?>) c.stream().filter(x -> x instanceof Float));
	}
}
