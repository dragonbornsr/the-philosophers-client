package eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;

public interface IBoundable {
	
	public boolean inBounds(double x, double y, boolean isTranslated);
	
	public default boolean inBounds(double x, double y) {
		y = Display.HEIGHT - y;
		return inBounds(x, y, true);
	}

}
