package eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableListItem;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.Mission;

public class ScrollableListItemMission extends ScrollableListItem {

	private TrueTypeFont ttf;
	private Mission mission;
	Color color = new Color(1f, 1f,1f,1f);
	private int textheight = 36;
	Texture image;
	
	
	public ScrollableListItemMission(ScrollableList parentScrollableList, Mission mission) {
		super(parentScrollableList);
		this.mission = mission;
		this.width = parentScrollableList.getWidth();
		this.height = 128;
		this.ttf = new TrueTypeFont("resources/fonts/DIOGENES.ttf", textheight);
		this.texture = ResourceManager.getTexture("resources/MissionItemBackground.png");
		this.image = ResourceManager.getTexture(mission.getMissionLocation() + "/mission.png");
		this.updateTextureCorners();
	}

	@Override
	public void draw(int pos, int x, int y) {
		 GL11.glPushMatrix();
		 texture.bind();
		 GL11.glColor4f(1f,1f, 1f, 1f);
		 GL11.glBegin(GL11.GL_QUADS);
		 	GL11.glTexCoord2f(c1.getX(), c1.getY());
		    GL11.glVertex2f(x, y);
		    GL11.glTexCoord2f(c2.getX(), c2.getY());
			GL11.glVertex2f(x + width, y);
			GL11.glTexCoord2f(c3.getX(), c3.getY());
			GL11.glVertex2f(x + width, y + height);
			GL11.glTexCoord2f(c4.getX(), c4.getY());
			GL11.glVertex2f(x, y + height);
		GL11.glEnd();
		GL11.glColor4f(1f,1f, 1f, 1f);
		GL11.glPopMatrix();
		
		 GL11.glPushMatrix();
		 image.bind();
		 GL11.glColor4f(1f,1f, 1f, 1f);
		 GL11.glBegin(GL11.GL_QUADS);
		 	GL11.glTexCoord2f(0, 1);
		    GL11.glVertex2f(x, y);
		    GL11.glTexCoord2f(1, 1);
			GL11.glVertex2f(x + this.height, y);
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2f(x + this.height, y + this.height);
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(x, y + this.height);
		GL11.glEnd();
		GL11.glColor4f(1f,1f, 1f, 1f);
		GL11.glPopMatrix();
		
		ttf.draw(this.mission.getMissionName(), x + 5 + this.height, y + this.height - this.textheight, color);
		ttf.draw(this.mission.getMissionVersion(), x + 5 + this.height, y + this.height - 2*(this.textheight), color);
		ttf.draw(this.mission.getMissionAuthorsAsString(), x + 5 + this.height, y + this.height - 3*(this.textheight), color);
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.width = this.parentScrollableList.getWidth(); 
		this.updateTextureCorners();
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean inBounds(double x, double y, boolean isTranslated) {
		return super.inBounds(x, y);
	}

	@Override
	public void draw(float delta) {
		// Not Used
		
	}

	@Override
	public void finalize() {
		
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		this.parentScrollableList.setSelectedItem(this);
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		
	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {
		
	}

	/**
	 * @return the mission
	 */
	public Mission getMission() {
		return mission;
	}
}
