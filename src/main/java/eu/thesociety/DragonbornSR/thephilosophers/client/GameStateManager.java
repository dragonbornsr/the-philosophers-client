package eu.thesociety.DragonbornSR.thephilosophers.client;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GameLogicGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.Background;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.DebugGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.HomeGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.MPLobbyCreationGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.MPLobbyGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.MPLobbySelectionGui;

public class GameStateManager {
	
	private static GameStateManager singleton;
	
	/**
	 * Holding most important gui objects in variables to prevent them to get lost in 
	 * drawableGameObjects list where they are hard to find.
	 */
	@SuppressWarnings("unused")
	private static HomeGui home;
	@SuppressWarnings("unused")
	private static Background background;
	private static DebugGui dbgui;
	@SuppressWarnings("unused")
	private static MPLobbySelectionGui mplobbyselection;
	@SuppressWarnings("unused")
	private static MPLobbyCreationGui mplobbycreation;
	@SuppressWarnings("unused")
	private static MPLobbyGui mplobby;
	
	private static GameState currentGameState = null;
	
	public static enum GameState {
		HOME(home = new HomeGui()),
		SETTINGS(null),
		MP_LOBBY_SELECTION(mplobbyselection = new MPLobbySelectionGui()),
		MP_LOBBY_CREATION(mplobbycreation = new MPLobbyCreationGui()),
		MP_LOBBY(mplobby = new MPLobbyGui()),
		MP_GAME(null),
		CREDITS(null);
		
		private GameLogicGui gui;
	    GameState(GameLogicGui gui) { 
	    	this.gui = gui; 
	    }
	    public GameLogicGui getValue() { return gui; }
	}
	
	/**
	 * Set up default game menu and background.
	 */
	private GameStateManager() {
		ResourceManager.instance();
		background = new Background();
		
		GameStateManager.currentGameState = GameState.HOME;
		
		dbgui = new DebugGui();
		dbgui.setVisible(false);
	}
	
	public static GameStateManager instance() {
		if (singleton != null) 
			return singleton;
		singleton = new GameStateManager();
		return singleton;
	}

	/**
	 * @return the currentGameState
	 */
	public GameState getCurrentGameState() {
		return currentGameState;
	}

	/**
	 * @param currentGameState the currentGameState to set
	 */
	public void setCurrentGameState(GameState newGameState) {
		GameStateManager.currentGameState.getValue().stop();
		GameStateManager.currentGameState = newGameState;
		GameStateManager.currentGameState.getValue().resume();
	}
}
