package eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableListItem;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Lobby;

public class ScrollableListItemServerSelectionServer extends ScrollableListItem {

	private TrueTypeFont ttf;
	private Lobby lobby;
	Color color = new Color(1f, 1f,1f,1f);
	
	
	public ScrollableListItemServerSelectionServer(ScrollableList parentScrollableList, Lobby lobby) {
		super(parentScrollableList);
		this.lobby = lobby;
		this.width = parentScrollableList.getWidth();
		this.height = 30;
		this.ttf = new TrueTypeFont("resources/fonts/DIOGENES.ttf", 32);
		this.texture = ResourceManager.getTexture("resources/ServerListItemBackground.png");
		this.updateTextureCorners();
	}

	@Override
	public void draw(int pos, int x, int y) {
		 GL11.glPushMatrix();
		 texture.bind();
		 GL11.glColor4f(1f,1f, 1f, 1f);
		 GL11.glBegin(GL11.GL_QUADS);
		 	GL11.glTexCoord2f(c1.getX(), c1.getY());
		    GL11.glVertex2f(x, y);
		    GL11.glTexCoord2f(c2.getX(), c2.getY());
			GL11.glVertex2f(x + width, y);
			GL11.glTexCoord2f(c3.getX(), c3.getY());
			GL11.glVertex2f(x + width, y + height);
			GL11.glTexCoord2f(c4.getX(), c4.getY());
			GL11.glVertex2f(x, y + height);
		GL11.glEnd();
		GL11.glColor4f(1f,1f, 1f, 1f);
		GL11.glPopMatrix();
		
		ttf.draw(pos + "", x + 5, y+ 6, color);
		ttf.draw(lobby.getLobbyHostName(), x + 55, y+ 6, color);
		ttf.draw(lobby.getMissionName(), x + 200, y+ 6, color);
		ttf.draw(lobby.getLobbyMemberCount() + "/" + lobby.getLobbyMaxMembers(), x + 800, y+ 6, color);
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.width = this.parentScrollableList.getWidth(); 
		this.updateTextureCorners();
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean inBounds(double x, double y, boolean isTranslated) {
		// TODO Auto-generated method stub
		return super.inBounds(x, y);
	}

	@Override
	public void draw(float delta) {
		// Not Used
		
	}

	@Override
	public void finalize() {
		// Not Used
		
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		this.parentScrollableList.setSelectedItem(this);
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {
		// TODO Auto-generated method stub
	}
	
	public Lobby getLobby() {
		return this.lobby;
	}

}
