package eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Gui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.Mission;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.MissionLoader;

public class ScrollableListMissions extends ScrollableList {
	
	public static ScrollableListMissions instance;

	public ScrollableListMissions(Gui parentGui) {
		super(parentGui, 100, 200, Display.WIDTH - 200,Display.HEIGHT - 300, new Texture("resources/white.png"));
		instance = this;
	}

	@Override
	public void onDisplayResized(int width, int height) {
		super.onDisplayResized(width, height);
		this.width = width - 200;
		this.height = height - 300;
		if (!this.items.isEmpty()) {
			int error = this.height % this.items.get(0).getHeight();
			this.height -= error;
		}
	}
	
	public void updateList() {
		this.items.clear();
		for (Mission mission : MissionLoader.getAvailableMissions()) {
			this.items.add(new ScrollableListItemMission(this, mission));
		}
		if (!this.items.isEmpty()) {
			int error = this.height % this.items.get(0).getHeight();
			this.height -= error;
		}
	}
}
