package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import java.awt.Color;

import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GameLogicGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TextButton;

public class HomeGui extends GameLogicGui  {
	
	TextButton startGameBtn, settingsBtn;
	
	int p = 1000;
	
	public HomeGui() {
		this.setSize(400, 500);
		this.setPosition(Display.WIDTH / 2 - width/2, Display.HEIGHT / 2 - height/2);
		
		this.setBackground(ResourceManager.getTexture("resources/white.png"));
		this.setBackgroundTransparency(0.0f);
		this.register(PRIORITY_HIGH);
		
		this.startGameBtn = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("buttons"))
				.setSizeAndPosition(10, height - 80, 380, 70, 0, 70));
		
		this.startGameBtn.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(70)
				.setText("Multiplayer");
				
		this.startGameBtn.setVisible(true);
		
		this.settingsBtn = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("buttons"))
				.setSizeAndPosition(10, height - 160, 380, 70, 0, 70));
		
		this.settingsBtn.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(70)
				.setText("Settings");
				
		this.settingsBtn.setVisible(true);

	}

	@Override
	public void buttonPressed(Button button) {

	}
	
	@Override
	public void buttonReleased(Button button) {
		if (button.equals(this.startGameBtn)) {
			GameStateManager.instance().setCurrentGameState(GameStateManager.GameState.MP_LOBBY_SELECTION);
		}
	}

	@Override
	public void textBoxChanged(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.setPosition(width / 2 - this.width/2, height / 2 - this.height/2);
	}
	
	@Override
	public void update(float delta) {

	}
	
	@Override
	public void draw(float delta) {
		super.draw(delta);
	}

	@Override
	public void resume() {
		this.setVisible(true);
		this.setRunning(true);
	}

	@Override
	public void stop() {
		this.setVisible(false);
		this.setRunning(false);
	}

	@Override
	public void reset() {
		this.resume();
	}		
}