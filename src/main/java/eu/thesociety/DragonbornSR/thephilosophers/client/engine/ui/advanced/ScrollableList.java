package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Gui;

public abstract class ScrollableList extends DrawableGameObject implements IMouseEvent { 
	
	protected Gui parentGui;
	protected Texture background;
	protected int x, y, width, height;
	protected float backgroundTransparency = 0.1f;
	
	protected ScrollableListItem selectedItem = null;
	
	protected ArrayList<ScrollableListItem> items = new ArrayList<ScrollableListItem>();
	protected int first = 0;
	
	public ScrollableList(Gui parentGui, int x, int y, int width, int height, Texture background) {
		this.parentGui = parentGui;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.background = background;
		this.register(parentGui.priority + 1);
	}
	
	@Override
	public void draw(float delta) {
			 GL11.glPushMatrix();
			 background.bind();
			 GL11.glColor4f(1f,1f, 1f, backgroundTransparency);
			 GL11.glBegin(GL11.GL_QUADS);
			 	GL11.glTexCoord2f(0.0f, 0.0f);
			    GL11.glVertex2f(this.getX(), this.getY());
			    GL11.glTexCoord2f((float)width / background.getWidth(), 0.0f);
				GL11.glVertex2f(this.getX() + width, this.getY());
				GL11.glTexCoord2f((float)width / background.getWidth(), (float)height / background.getHeight());
				GL11.glVertex2f(this.getX() + width, this.getY() + height);
				GL11.glTexCoord2f(0, (float)height / background.getHeight());
				GL11.glVertex2f(this.getX(), this.getY() + height);
			GL11.glEnd();
			GL11.glColor4f(1f,1f, 1f, 1f);
			GL11.glPopMatrix();
			if (!items.isEmpty()) {
				int offset = this.height - items.get(0).getHeight();
				int remaining = height / items.get(0).getHeight();
				for (int pos = first; pos < items.size(); pos++) {
					if (items.size() > pos && remaining > 0 && pos >= 0) {
						items.get(pos).draw(pos, this.getX(), this.getY() + offset);
						offset -= items.get(pos).getHeight();
						remaining --;
					} else break;
				}
			}
	}

	@Override
	public void onDisplayResized(int width, int height) {
		for (ScrollableListItem i : items)
			i.onDisplayResized(width, height);
	}

	@Override
	public void update(float delta) {
		for (ScrollableListItem item : this.items)
			item.update(delta);
		
	}

	@Override
	public void finalize() {

	}

	@Override
	public boolean inBounds(double x, double y, boolean isTranslated) {
		if (this.getX() <= x  && this.getX() + this.width >= x 
				&& this.getY() <= y && this.getY() + height >= y) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		ScrollableListItem item = this.findItemAtPos(x, y);
		if (item != null) {
			item.mousePressed(x, y, button, mods);
		}
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		ScrollableListItem item = this.findItemAtPos(x, y);
		if (item != null) {
			item.mouseReleased(x, y, button, mods);
		}
	}
	
	private ScrollableListItem findItemAtPos(double x, double y) {
		if (!items.isEmpty()) {
			int offset = this.height - items.get(0).getHeight();
			int remaining = findHeightOfVisibleRows() / items.get(0).getHeight();
			for (int pos = first; pos < items.size(); pos++) {
				if (pos < 0) continue;
				if (items.size() > pos && remaining > 0) {
					if (x > this.getX() && x < this.getX() + this.getWidth() &&
							y > this.getY() + offset
							&& y < this.getY() + offset + items.get(pos).getHeight()) {
						return items.get(pos);
					}
					if (pos >= 0)
						offset -= items.get(pos).getHeight();
					remaining --;
				} else return null;
			}
		}
		return null;
	}
	
	private int findHeightOfVisibleRows() {
		int h = 0;
		for (ScrollableListItem i : items) {
			h += i.height;
			if (h > this.height) return this.height;
		}
		return h;
	}

	@Override
	public void mouseMoved(double x, double y) {
		ScrollableListItem item = this.findItemAtPos(x, y);
		if (item != null) {
			item.mouseMoved(x, y);
		}
	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {
		if (yDirection > 0 && !this.canScrollUp()) return;
		else if (yDirection < 0 && !this.canScrollDown()) return;
		else if (items.isEmpty()) return;
		first -= yDirection;
		
		if (first > items.size() - height / items.get(0).getHeight()) {
			first = items.size() - height / items.get(0).getHeight();
		}
		if (first < 0) first = 0;
	}
	
	public int getX() {
		return parentGui.getX() + this.x;
	}
	
	public int getY() {
		return parentGui.getY() + this.y;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	@Override
	public boolean isEnabled() {
		return parentGui.isEnabled() && super.isEnabled();
	}
	
	@Override
	public boolean isVisible() {
		return parentGui.isVisible() && super.isVisible();
	}
	
	public int getListSize() {
		return this.items.size();
	}
	
	public int getDisplayableItemCount() {
		if (this.items.size() > 0) {
			return this.height / this.items.get(0).getHeight();
		}
		return 0;
	}

	public void setSelectedItem(ScrollableListItem scrollableListItem) {
		this.selectedItem = scrollableListItem;
	}

	public ScrollableListItem getSelectedItem() {
		return this.selectedItem;
	}
	
	public void clear() {
		this.items.clear();
	}
	
	public boolean canScrollDown() {
		return true;
	}
	
	public boolean canScrollUp() {
		return (this.first > 0);
	}
}
