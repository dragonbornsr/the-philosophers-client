package eu.thesociety.DragonbornSR.thephilosophers.client.engine.fx.particles;

import java.util.ArrayList;
import java.util.TreeSet;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;

public abstract class ParticleSystem extends DrawableGameObject {

	protected ArrayList<ParticlePath> paths = new ArrayList<ParticlePath>();
	protected ArrayList<float[]> particles = new ArrayList<float[]>();
	protected TreeSet<Integer> particlesToDelete = new TreeSet<Integer>();
	protected Texture texture;
	protected float fadeInSpeed = 0.001f;
	
	protected enum PARTICLE {
		X(0),
		Y(1),
		W(2),
		H(3),
		SPEEDX(4),
		SPEEDY(5),
		PATH(6),
		SUMDELTA(7),
		c11(8), c12(9), c21(10), c22(11), c31(12), c32(13), c41(14), c42(15), SEED(16), SCALE(17), TRANSPARENCY(18), FADE(19);
		
		private int value;    

		  private PARTICLE(int value) {
		    this.value = value;
		  }

		  public int getValue() {
		    return value;
		  }
	}

	public ParticleSystem() {

	}

	@Override
	public void draw(float delta) {

		for (float[] particle : particles) {
			float X = particle[PARTICLE.X.getValue()];
			float Y = particle[PARTICLE.Y.getValue()];
			float W = particle[PARTICLE.W.getValue()];
			float H = particle[PARTICLE.H.getValue()];
			float scale = particle[PARTICLE.SCALE.getValue()];
			GL11.glPushMatrix();
			texture.bind();
			GL11.glColor4f(1f, 1f, 1f, particle[PARTICLE.TRANSPARENCY.getValue()]*particle[PARTICLE.FADE.getValue()]);
			GL11.glBegin(GL11.GL_QUADS);
			
			GL11.glTexCoord2f(particle[PARTICLE.c11.getValue()], particle[PARTICLE.c12.getValue()]);
			GL11.glVertex2f(X, Y);

			GL11.glTexCoord2f(particle[PARTICLE.c21.getValue()], particle[PARTICLE.c22.getValue()]);
			GL11.glVertex2f(X + W*scale, Y);

			GL11.glTexCoord2f(particle[PARTICLE.c31.getValue()], particle[PARTICLE.c32.getValue()]);
			GL11.glVertex2f(X + W*scale, Y + H*scale);

			GL11.glTexCoord2f(particle[PARTICLE.c41.getValue()], particle[PARTICLE.c42.getValue()]);
			GL11.glVertex2f(X, Y + H*scale );

			GL11.glEnd();
			GL11.glColor4f(1f, 1f, 1f, 1f);
			GL11.glPopMatrix();
		}

	}

	@Override
	public void onDisplayResized(int width, int height) {
		
	}

	@Override
	public void update(float delta) {
		if (delta > 10) delta = 1;
		
		for (int p = 0; p < particles.size(); p++) {
			Vector2f location = this.paths.get((int)particles.get(p)[PARTICLE.PATH.getValue()]).getParticleLocation(particles.get(p), delta);
			particles.get(p)[PARTICLE.X.getValue()] = location.getX();
			particles.get(p)[PARTICLE.Y.getValue()] = location.getY();
			particles.get(p)[PARTICLE.SUMDELTA.getValue()] += delta;
			if (particles.get(p)[PARTICLE.FADE.getValue()] < 1f)
				particles.get(p)[PARTICLE.FADE.getValue()] += delta*this.fadeInSpeed;
			if (particles.get(p)[PARTICLE.X.getValue()] < -1*particles.get(p)[PARTICLE.W.getValue()]*particles.get(p)[PARTICLE.SCALE.getValue()] ||
				particles.get(p)[PARTICLE.X.getValue()] > Display.WIDTH ||
				particles.get(p)[PARTICLE.Y.getValue()] < -1*particles.get(p)[PARTICLE.H.getValue()]*particles.get(p)[PARTICLE.SCALE.getValue()] ||
				particles.get(p)[PARTICLE.Y.getValue()] > Display.HEIGHT) {
				this.particlesToDelete.add(Integer.valueOf(p));
			}
		}
		
		while (!particlesToDelete.isEmpty()) {
			particles.remove(particlesToDelete.pollLast().intValue());
		}
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub

	}
	
	public void updateTextureCorners() {
		
	}
	
	protected int getRandomPath() {
		return 0 + (int)(Math.random() * (paths.size()));
	}
}
