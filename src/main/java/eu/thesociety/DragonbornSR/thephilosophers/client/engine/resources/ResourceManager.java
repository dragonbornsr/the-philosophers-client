package eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.CoreObject;

public class ResourceManager extends CoreObject {
	
	public static ResourceManager singleton;
	
	protected List<String> listOfResources = new ArrayList<String>();
	public static boolean load = true;
	protected int pointer = 0;
	public static String lastLoadingItem;
	
	private static HashMap<String, Texture> textures = new HashMap<String, Texture>();
	private static Texture missing;
	
	private ResourceManager() {
		register();
		loadResources();
		getResourceNames("resources");
	}
	
	public static ResourceManager instance() {
		if (singleton != null) 
			return singleton;
		singleton = new ResourceManager();
		return singleton;
	}
	

	@Override
	public void update(float delta) {
		if (load) {
				load(listOfResources.get(pointer));
			pointer++;
			if (pointer >= listOfResources.size())
				load = false;
		}
	}
	
	private void loadResources() {
		textures.put("background", new Texture("resources/background.png"));
		textures.put("logo", new Texture("resources/logo.png"));
		textures.put("buttons", new Texture("resources/buttons.png"));
		ResourceManager.setMissingTexture(new Texture("resources/missing.png"));
	}

	public static Texture getTexture(String key) {
		if (textures.containsKey(key))
			return textures.get(key);
		textures.put(key, new Texture(key));
		Texture t = textures.get(key);
		if (t != null)
			return t;
		return missing;
	}

	/**
	 * @return the missing
	 */
	public static Texture getMissingTexture() {
		return missing;
	}

	/**
	 * @param missing the missing to set
	 */
	public static void setMissingTexture(Texture missing) {
		ResourceManager.missing = missing;
	}
	
	/**
	 * Load a resource
	 * @param resStr
	 * @throws IOException
	 * @throws FontFormatException
	 */
	protected void load(String resStr) {
		if (resStr.length() > 0) {
			String resIndexName = null;
			String resExtentionName = null;
			String[] fileparts = resStr.split("\\\\");
			resIndexName = fileparts[fileparts.length - 1];
			int pos = resIndexName.lastIndexOf(".");
			if (pos > 0) {
				resExtentionName = resIndexName.substring(pos + 1,
						resIndexName.length());
				resIndexName = resIndexName.substring(0, pos);
			}
			
			if (resExtentionName.equals("png")) {
				textures.put(resIndexName, new Texture(resStr));
			}
		}
	}
	
	/**
	 * Get all resources recursively in the directory.
	 * @param sDir
	 */
	public void getResourceNames(String sDir) {
		File[] faFiles = new File(sDir).listFiles();
		for (File file : faFiles) {
			if (file.getName().matches("^(.*?)")) {
				if (!file.isDirectory())
					listOfResources.add(file.getAbsolutePath());
			}
			if (file.isDirectory()) {
				getResourceNames(file.getAbsolutePath());
			}
		}
	}
}
