package eu.thesociety.DragonbornSR.thephilosophers.client.engine.fx.particles;

import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;

public abstract class ParticlePath {
	public abstract Vector2f getParticleLocation(float[] particle, float delta);
	
	protected byte getBit(int position, byte byte1) {
		return (byte) ((byte1 >> position) & 1);
	}
}
