package eu.thesociety.DragonbornSR.thephilosophers.client.utils;

public class ArraysUtil {
	
	public static String stringArraySeparatedBy(String[] parts, String separator) {
		String result = "";
		for (String part : parts)
			result += part + separator;
		if (result.length() > 0) result = result.substring(0, result.length() - separator.length());
		return result;
	}

}
