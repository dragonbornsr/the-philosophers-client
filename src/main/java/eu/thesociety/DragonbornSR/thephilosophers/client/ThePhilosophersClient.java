package eu.thesociety.DragonbornSR.thephilosophers.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.codedisaster.steamworks.SteamAPI;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.KeyboardController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.MouseController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.TickHandler;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.CoreObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.GameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Matchmaking;

public class ThePhilosophersClient extends Thread {
	
	public static final String gamekey = "The Philosophers Client by Oliver Tiit";
	public static final String apikey = "538285E519615CE1DCFEB0460D94D145";
	final static Logger logger = Logger.getLogger(ThePhilosophersClient.class);
	
	public static Display display;
	public static TickHandler th = new TickHandler();

	public static List<DrawableGameObject> listOfDrawableGameObjects = new ArrayList<DrawableGameObject>();
	public static List<GameObject> listOfGameObjects = new ArrayList<GameObject>();

	public static List<DrawableGameObject> listOfDeletedDrawableGameObjects = new ArrayList<DrawableGameObject>();
	public static List<GameObject> listOfDeletedGameObjects = new ArrayList<GameObject>();

	public static List<CoreObject> listOfCoreObjects = new ArrayList<CoreObject>();
	
	Matchmaking lobby;
	
	public static DrawableGameObject focused = null;
	public static DrawableGameObject hovering = null;
	
	public static void main(String[] args) {
		
		ThePhilosophersClient game = new ThePhilosophersClient();
		
		game.start();
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);

		
		while (game.isAlive()) {
			String s = in.nextLine();
			if (s.startsWith("msg")) {
				Matchmaking.instance().getSteamMatchmaking().sendLobbyChatMsg(Matchmaking.getLobbyId(), s.substring(4));
			}
		}
	}
	@Override
	public void run() {
		boolean gui = true;
		if (gui) {
			preInit();
			init();
			postInit();
			while (true) {
				try {
					th.handle(this);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				SteamAPI.runCallbacks();
			}
		} else {
			if (!SteamAPI.init()) {
				logger.fatal("Cannot start the application. Problems with connecting Steam!");
				System.exit(0);
			}
			while (true) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				SteamAPI.runCallbacks();
			}
			
		}
	}
	
	private void preInit() {
		
	}
	
	public void init() {
		if (!SteamAPI.init()) {
			logger.fatal("Cannot start the application. Problems with connecting Steam!");
			System.exit(0);
			
			
		}
		if (SteamAPI.isSteamRunning()) {
			display = new Display();
			display.run();
		}
		
		th.setMaxTPS(60);
	}

	private void postInit() {
		MouseController.instance();
		KeyboardController.instance();
		GameStateManager.instance();
	}

	public void updateLoop() {
		if (display.isCloseRequested()) {
			SteamAPI.shutdown();
			exitGame();
		} else {
			for (DrawableGameObject o : listOfDeletedDrawableGameObjects) {
				listOfDrawableGameObjects.remove(o);
			}
			listOfDeletedDrawableGameObjects.clear();
			for (GameObject o : listOfDeletedGameObjects) {
				listOfDeletedGameObjects.remove(o);
			}
			listOfDeletedGameObjects.clear();

			for (CoreObject co : listOfCoreObjects) {
				co.update(th.getDelta());
			}
			for (GameObject go : listOfGameObjects) {
				if (go.isEnabled())
					go.update(th.getDelta());
			}
			for (DrawableGameObject dgo : listOfDrawableGameObjects) {
				if (dgo.isEnabled())
					dgo.update(th.getDelta());
			}
		}
		
		
	}

	public void drawLoop() {
		display.render();
		
	}	
	
	public void exitGame() {
		logger.debug("Exit Game Called");
		for (DrawableGameObject dgo : listOfDrawableGameObjects) {
			dgo.finalize();
		}
		for (GameObject go : listOfGameObjects) {
			go.finalize();
		}
		display.destroy();
		System.exit(0);
	}
	
	public static boolean isFocused(DrawableGameObject o) {
		if (o == focused)
			return true;
		return false;
	}

}