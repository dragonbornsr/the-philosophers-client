package eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces;

public interface IMouseEvent extends IBoundable {

	boolean isDeleted();

	void mousePressed(double x, double y, int button, int mods);

	void mouseReleased(double x, double y, int button, int mods);

	void mouseMoved(double x, double y);
	
	void mouseScrolled (double xDirection, double yDirection);

}
