package eu.thesociety.DragonbornSR.thephilosophers.client.engine;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.annotations.Placeholder;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.CoreObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.GameObject;

public class MouseController extends CoreObject {
	
	private static final int MOUSE_PRESS = 1;
	private static final int MOUSE_RELEASE = 0;
	
	
	private static MouseController singleton;
	
	protected static List<GameObject> listOfMouseEventListeners = new ArrayList<GameObject>();
	public static List<GameObject> listOfDeleted = new ArrayList<GameObject>();
	
	@SuppressWarnings("unused")
	private static GLFWMouseButtonCallback mouseClickCallback;
	@SuppressWarnings("unused")
	private static GLFWCursorPosCallback mouseMoveCallback;
	@SuppressWarnings("unused")
	private static GLFWScrollCallback mouseScrolledCallback;
	
	private double mx = 0, my = 0;
	
	private MouseController() {
		
	}
	
	public static MouseController instance() {
		if (singleton == null) 
			singleton = new MouseController();
		return singleton;
	}

	@Override
	public void update(float delta) {
		for (GameObject o : MouseController.getListOfMouseEventListeners())
			if (((IMouseEvent) o)
					.isDeleted()) {
				MouseController.listOfDeleted.add(o);
			}
		for (GameObject o : MouseController.listOfDeleted)
			if (((IMouseEvent) o)
					.isDeleted()) {
				MouseController.getListOfMouseEventListeners().remove(o);
			}
		MouseController.listOfDeleted.clear();
	}

	protected void forwardMousePressed(double x, double y, int button, int mods) {
		for (GameObject o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(x, y) && (o.isEnabled())) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).isVisible()) {
						((IMouseEvent) o)
						.mousePressed(x, Display.HEIGHT - y, button, mods);
						return;
					}
				} else {
				((IMouseEvent) o)
						.mousePressed(x, y, button, mods);
					return;
				}
			}
	}

	protected void forwardMouseReleased(double x, double y, int button, int mods) {
		for (Object o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(x, y) && (((GameObject) o).isEnabled())) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).isVisible()) {
						((IMouseEvent) o)
						.mouseReleased(x, Display.HEIGHT - y, button, mods);
						return;
					}
				} else {
				((IMouseEvent) o)
						.mouseReleased(x , y, button, mods);
					return;
				}
			}
	}
	
	protected void forwardMouseMoved(double x, double y) {
		boolean foundFirst = false;
		mx = x; my = y;
		for (Object o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(x, y) && (((GameObject) o).isEnabled())) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).isVisible()) {
						if (!foundFirst) {
							ThePhilosophersClient.hovering = (DrawableGameObject) o;
							foundFirst = true;
						}
						((IMouseEvent) o)
						.mouseMoved(x, Display.HEIGHT - y);
						return;
					}
				} else {
				((IMouseEvent) o)
						.mouseMoved(x , y);
					return;
				}
			}
		ThePhilosophersClient.hovering = null;
	}
	
	protected void forwardMouseScrolled(double xoffset, double yoffset) {
		for (GameObject o : getListOfMouseEventListeners())
			if (((IMouseEvent) o).inBounds(mx, my) && (o.isEnabled())) {
				if (o instanceof DrawableGameObject) {
					if (((DrawableGameObject)o).isVisible()) {
						((IMouseEvent) o)
						.mouseScrolled(xoffset, yoffset);
						return;
					}
				} else {
				((IMouseEvent) o)
						.mouseScrolled(xoffset, yoffset);
					return;
				}
			}
	}
	
	/**
	 * Register a GameObject to be a IMouseEvent Listener.  Make sure that
	 * GameObject implements IMouseEvent.
	 * @param listener
	 * @param priority
	 */
	@Placeholder
	public void registerMouseEventListener(GameObject listener, int priority) {
		boolean added = false;
		if (listOfMouseEventListeners.isEmpty()) {
			listOfMouseEventListeners.add(0, listener);
			added = true;
		} else {
			for (GameObject mel : listOfMouseEventListeners) {
				if (mel.priority <= priority) {
					int index = listOfMouseEventListeners.indexOf(mel);
					listOfMouseEventListeners.add(index, listener);
					added = true;
					break;
				}
			}
			if (!added) {
				listOfMouseEventListeners.add(listener);
			}
		}
	}
	
	/**
	 * Change priority of a button. Use this when bringing a GUI/Button forward relative to any other.
	 * @param listener
	 * @param priority
	 */
	public void changePriority(GameObject listener, int priority) {
		listOfMouseEventListeners.remove(listener);
		registerMouseEventListener(listener, priority);
	}

	public static List<GameObject> getListOfMouseEventListeners() {
		return listOfMouseEventListeners;
	}
	
	static {
		GLFW.glfwSetMouseButtonCallback(ThePhilosophersClient.display.getDisplayId(), mouseClickCallback = new GLFWMouseButtonCallback() {
			@Override
		    public void invoke(long window, int button, int action, int mods) {
				DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
				DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
		        GLFW.glfwGetCursorPos(ThePhilosophersClient.display.getDisplayId(), x, y);
		        if (x != null && y != null) {
		        	if (action == MOUSE_PRESS)
		        		MouseController.instance().forwardMousePressed(x.get(), y.get(), button, mods);
		        	else if (action == MOUSE_RELEASE)
		        		MouseController.instance().forwardMouseReleased(x.get(), y.get(), button, mods);
		        }
		    }
		});
		
		GLFW.glfwSetCursorPosCallback(ThePhilosophersClient.display.getDisplayId(), mouseMoveCallback = new GLFWCursorPosCallback() {
		    @Override
		    public void invoke(long window, double x, double y) {
		    	MouseController.instance().forwardMouseMoved(x, y);
		    }
		});
		
		GLFW.glfwSetScrollCallback(ThePhilosophersClient.display.getDisplayId(), mouseScrolledCallback = new GLFWScrollCallback() {
			@Override
			public void invoke(long window, double xoffset, double yoffset) {
				MouseController.instance().forwardMouseScrolled(xoffset, yoffset);
			}
		});
	}
}
