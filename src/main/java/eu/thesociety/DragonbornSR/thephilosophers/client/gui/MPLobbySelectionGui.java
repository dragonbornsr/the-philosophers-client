package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import java.awt.Color;

import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GameLogicGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TextButton;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Matchmaking;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.Mission;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.MissionLoader;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListItemServerSelectionServer;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListServerSelection;

public class MPLobbySelectionGui extends GameLogicGui {
	
	private TrueTypeFont ttf;
	private Color color = new Color(1f,1f,1f,0.5f);
	private int fontsize = 36;
	
	String debugList = "";
	
	int i = 300;
	int p = 0;
	
	Matchmaking lobby;
	
	ScrollableList serverlist;
	
	TextButton joinRoomButton, createRoomButton, backButton;
	
	public MPLobbySelectionGui() {
		ttf = new TrueTypeFont("resources/fonts/DIOGENES.ttf", fontsize);
		this.setSize(Display.WIDTH, Display.HEIGHT);
		Texture t = new Texture("resources/white.png");
		this.setBackgroundTransparency(0.0f);
		this.setBackground(t);
		this.setPosition(0, 0);
		this.register(PRIORITY_MEDIUM);
		lobby = Matchmaking.instance();
		this.setVisible(false);
		this.setEnabled(false);
		
		this.serverlist = new ScrollableListServerSelection(this);
		
		this.joinRoomButton = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("resources/missing.png"))
				.setSizeAndPosition(this.width - 250, 150, 150, 40, 0, 70));
		
		this.joinRoomButton.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(fontsize)
				.setText("Join Room");
				
		this.joinRoomButton.setVisible(true);
		
		this.createRoomButton = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("resources/missing.png"))
				.setSizeAndPosition(this.width - 410, 150, 150, 40, 0, 70));
		
		this.createRoomButton.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(fontsize)
				.setText("Create Room");
				
		this.createRoomButton.setVisible(true);
		
		this.backButton = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("resources/missing.png"))
				.setSizeAndPosition(this.width - 550, 150, 130, 40, 0, 70));
		
		this.backButton.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(fontsize)
				.setText("Back");
				
		this.backButton.setVisible(true);
		
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
	}
	
	@Override
	public void draw(float delta) {
		if (this.isRunning()) {
			super.draw(delta);
			ttf.draw(debugList, 100, Display.HEIGHT - 100, color);
		}
	}

	@Override
	public void resume() {
		this.setVisible(true);
		this.setEnabled(true);
		this.setRunning(true);
		ScrollableListServerSelection.instance.clear();
		//Matchmaking.instance().getSteamMatchmaking()
		//	.addRequestLobbyListStringFilter(Lobby.KEY_GAMEKEY, Crypto.MD5(ThePhilosophersClient.gamekey), LobbyComparison.Equal);
		ScrollableListServerSelection.instance.clear();
		Matchmaking.instance().getSteamMatchmaking().requestLobbyList();
	}

	@Override
	public void stop() {
		this.setVisible(false);
		this.setEnabled(false);
		this.setRunning(false);
		ScrollableListServerSelection.instance.clear();
	}

	@Override
	public void reset() {
		this.resume();
	}

	@Override
	public void buttonPressed(Button button) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void buttonReleased(Button button) {
		if (button.equals(this.joinRoomButton)) {
			if (serverlist.getSelectedItem() != null)
				lobby.getSteamMatchmaking().joinLobby(((ScrollableListItemServerSelectionServer)serverlist.getSelectedItem()).getLobby().getSteamID());
		} else if (button.equals(this.backButton)) {
			GameStateManager.instance().setCurrentGameState(GameState.HOME);
		} else if (button.equals(this.createRoomButton)) {
			GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY_CREATION);
		}
		
	}

	@Override
	public void textBoxChanged(GuiTextBox textBox) {
		
	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {
		
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.setSize(width, height);
		this.joinRoomButton.setPosition(this.width - 250, 150);
		this.createRoomButton.setPosition(this.width - 410, 150);
		this.backButton.setPosition(this.width - 550, 150);
	}
	
	public void createDevtestRoom() {
		Mission mission = MissionLoader.loadMission("devtest", false);
		Matchmaking.instance().makeGame(mission);
	}
}
