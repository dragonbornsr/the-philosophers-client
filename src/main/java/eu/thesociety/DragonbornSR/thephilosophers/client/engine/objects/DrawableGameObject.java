package eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.KeyboardController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.MouseController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IKeyEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;

public abstract class DrawableGameObject extends GameObject {

	/**
	 * Object name
	 */
	public String unlocalizedName = this.getClass().getName();
	/**
	 * Set if object will be drawn on the screen.
	 */
	private boolean visible = true;
	

	/**
	 * Draw with OpenGL NB! Please do not use this method to update any
	 * parameters anymore. Just plain drawing, because this might be
	 * multi-threaded and might cause synchronization problem.
	 */
	public abstract void draw(float delta);
	
	public abstract void onDisplayResized(int width, int height);

	/**
	 * Call this function to delete object next tick safely. This will call out
	 * finalize method that will save all nessecary data etc.
	 * 
	 */
	@Override
	public void unregister() {
		this.visible = false; // prevent draw on same tick!
		this.setEnabled(false);
		this.delete = true;
		finalize();
		ThePhilosophersClient.listOfDeletedDrawableGameObjects.add(this);
	}

	/**
	 * register Object to render/update loop with certain priority. Priority 0
	 * will be drawn first, but will stay under other layers!
	 */
	@Override
	public void register(int priority) {
		this.priority = priority;
		int index = 0;
		boolean added = false;
		if (ThePhilosophersClient.listOfDrawableGameObjects.isEmpty()) {
			ThePhilosophersClient.listOfDrawableGameObjects.add(0, this);
		} else {
			for (DrawableGameObject dgo : ThePhilosophersClient.listOfDrawableGameObjects) {
				if (dgo.priority >= priority) {
					index = ThePhilosophersClient.listOfDrawableGameObjects.indexOf(dgo);
					ThePhilosophersClient.listOfDrawableGameObjects.add(index, this);
					added = true;
					break;
				}
			}
			if (!added) 
				ThePhilosophersClient.listOfDrawableGameObjects.add(this);
		}
		/*
		 * Register all Event Handlers.
		 */
		if (this instanceof IMouseEvent) {
			MouseController.instance().registerMouseEventListener(this, priority);
		}
		if (this instanceof IKeyEvent) {
			KeyboardController.instance().registerKeyboardEventListener(this, priority);
		}
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}