package eu.thesociety.DragonbornSR.thephilosophers.client.types;

import java.util.Date;

import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamMatchmaking.ChatEntryType;

import eu.thesociety.DragonbornSR.thephilosophers.client.game.Matchmaking;

public class ChatMessage {
	
	private Date time;
	private SteamID user;
	private String message;
	
	public ChatMessage(SteamID user, String message, ChatEntryType chatEntryType) {
		this.user = user;
		this.message = message;
	}
	
	/**
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}
	/**
	 * @param time the time to set
	 */
	public void setTime(Date time) {
		this.time = time;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return Matchmaking.getCurrentLobby().getPlayer(user).getPlayerName();
	}
	/**
	 * @return the user
	 */
	public SteamID getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(SteamID user) {
		this.user = user;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
}
