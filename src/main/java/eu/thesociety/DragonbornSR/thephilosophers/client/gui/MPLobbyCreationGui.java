package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import java.awt.Color;

import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GameLogicGui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TextButton;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.MissionLoader;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListMissions;

public class MPLobbyCreationGui extends GameLogicGui {
	
	private TrueTypeFont ttf;
	private Color color = new Color(1f,1f,1f,0.5f);
	private int fontsize = 36;
	
	String debugList = "";
	
	int i = 300;
	int p = 0;
	
	private TextButton backButton;
	private ScrollableListMissions scrollableMissionList;
	
	public MPLobbyCreationGui() {
		ttf = new TrueTypeFont("resources/fonts/DIOGENES.ttf", fontsize);
		this.setSize(Display.WIDTH, Display.HEIGHT);
		Texture t = new Texture("resources/white.png");
		this.setBackgroundTransparency(0.0f);
		this.setBackground(t);
		this.setPosition(0, 0);
		this.register(PRIORITY_MEDIUM);
		this.setVisible(false);
		this.setEnabled(false);
		MissionLoader.refreshAvailableMissions();
		this.scrollableMissionList = new ScrollableListMissions(this);
		this.scrollableMissionList.updateList();
		
		this.backButton = (TextButton) (new TextButton(this)
				.setTexture(ResourceManager.getTexture("resources/missing.png"))
				.setSizeAndPosition(this.width - 550, 150, 130, 40, 0, 70));
		
		this.backButton.setDefaultTextColor(new Color(1f,1f,1f,1f))
				.setFocusedTextColor(new Color(0f,1f,1f,1f))
				.setFontname("resources/fonts/DIOGENES.ttf")
				.setFontsize(fontsize)
				.setText("Back");
		
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
	}
	
	@Override
	public void draw(float delta) {
		if (this.isRunning()) {
			super.draw(delta);
			ttf.draw(debugList, 100, Display.HEIGHT - 100, color);
		}
	}

	@Override
	public void resume() {
		this.setVisible(true);
		this.setEnabled(true);
		this.setRunning(true);
		MissionLoader.refreshAvailableMissions();
	}

	@Override
	public void stop() {
		this.setVisible(false);
		this.setEnabled(false);
		this.setRunning(false);
	}

	@Override
	public void reset() {
		this.resume();
	}

	@Override
	public void buttonPressed(Button button) {
		
		
	}

	@Override
	public void buttonReleased(Button button) {
		if (button.equals(this.backButton)) {
			GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY_SELECTION);
		}
	}

	@Override
	public void textBoxChanged(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDisplayResized(int width, int height) {
		this.setSize(width, height);
	}

	/**
	 * @return the scrollableMissionList
	 */
	public ScrollableListMissions getScrollableMissionList() {
		return scrollableMissionList;
	}
}
