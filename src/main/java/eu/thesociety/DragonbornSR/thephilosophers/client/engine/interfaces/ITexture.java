package eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces;

public interface ITexture {

	public void bind();

	public void unbind();
	
	
	public int getWidth();
	
	public int getHeight();
	
}
