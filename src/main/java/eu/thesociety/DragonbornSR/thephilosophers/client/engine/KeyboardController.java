package eu.thesociety.DragonbornSR.thephilosophers.client.engine;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWCharCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.annotations.Placeholder;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IKeyEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.CoreObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.GameObject;

public class KeyboardController extends CoreObject {
	
	private static KeyboardController singleton;
	
	protected static List<GameObject> listOfkeyboardEventListeners = new ArrayList<GameObject>();
	public static List<GameObject> listOfDeleted = new ArrayList<GameObject>();
	
	@SuppressWarnings("unused")
	private static GLFWMouseButtonCallback mouseClickCallback;
	@SuppressWarnings("unused")
	private static GLFWCursorPosCallback mouseMoveCallback;
	@SuppressWarnings("unused")
	private static GLFWKeyCallback keyCallback;
	@SuppressWarnings("unused")
	private static GLFWCharCallback charCallback;

	
	private KeyboardController() {
		GLFW.glfwSetInputMode(ThePhilosophersClient.display.getDisplayId(), GLFW.GLFW_STICKY_KEYS, 1);
	}
	
	public static KeyboardController instance() {
		if (singleton == null) 
			singleton = new KeyboardController();
		return singleton;
	}

	@Override
	public void update(float delta) {

		/*
		 * Avoid Concurrent Exceptions on removing while in the loop!
		 */
		for (GameObject o : KeyboardController.getlistOfKeyboardEventListeners())
			if (((IMouseEvent) o)
					.isDeleted()) {
				KeyboardController.listOfDeleted.add(o);
			}
		for (GameObject o : KeyboardController.listOfDeleted)
			if (((IMouseEvent) o)
					.isDeleted()) {
				KeyboardController.getlistOfKeyboardEventListeners().remove(o);
			}
		KeyboardController.listOfDeleted.clear();
	}

	protected void forwardKeyPressed(int key, int scancode, int mods) {
		for (Object o : getlistOfKeyboardEventListeners())
			if ((((GameObject) o).isEnabled())) {
				((IKeyEvent) o).keyPressed(key, scancode, mods);
			}
	}

	protected void forwardKeyReleased(int key, int scancode, int mods) {
		for (Object o : getlistOfKeyboardEventListeners())
			if ((((GameObject) o).isEnabled())) {
				((IKeyEvent) o).keyReleased(key, scancode, mods);
			}
	}
	
	protected void forwardCharacterTyped(int codepoint) {
		for (Object o : getlistOfKeyboardEventListeners())
			if ((((GameObject) o).isEnabled())) {
				((IKeyEvent) o).characterTyped(codepoint);
			}
	}
	
	/**
	 * Register a GameObject to be a IMouseEvent Listener.  Make sure that
	 * GameObject implements IMouseEvent.
	 * @param listener
	 * @param priority
	 */
	@Placeholder
	public void registerKeyboardEventListener(GameObject listener, int priority) {
		boolean added = false;
		if (listOfkeyboardEventListeners.isEmpty()) {
			listOfkeyboardEventListeners.add(0, listener);
			added = true;
		} else {
			for (GameObject mel : listOfkeyboardEventListeners) {
				if (mel.priority <= priority) {
					int index = listOfkeyboardEventListeners.indexOf(mel);
					listOfkeyboardEventListeners.add(index, listener);
					added = true;
					break;
				}
			}
			if (!added) {
				listOfkeyboardEventListeners.add(listener);
			}
		}
	}
	
	/**
	 * Change priority of a button. Use this when bringing a GUI/Button forward relative to any other.
	 * @param listener
	 * @param priority
	 */
	public void changePriority(GameObject listener, int priority) {
		listOfkeyboardEventListeners.remove(listener);
		registerKeyboardEventListener(listener, priority);
	}

	public static List<GameObject> getlistOfKeyboardEventListeners() {
		return listOfkeyboardEventListeners;
	}
	
	static {
		GLFW.glfwSetKeyCallback(ThePhilosophersClient.display.getDisplayId(), keyCallback = new GLFWKeyCallback() {
		    @Override
		    public void invoke (long window, int key, int scancode, int action, int mods) {
		    	if (action == GLFW.GLFW_PRESS)
		    		KeyboardController.instance().forwardKeyPressed(key, scancode, mods);
		    	else if (action == GLFW.GLFW_RELEASE)
		    		KeyboardController.instance().forwardKeyReleased(key, scancode, mods);
		    }
		});
		
		GLFW.glfwSetCharCallback(ThePhilosophersClient.display.getDisplayId(), charCallback = new GLFWCharCallback() {

			@Override
			public void invoke(long window, int codepoint) {
				KeyboardController.instance().forwardCharacterTyped(codepoint);
			}
			
		});
	}
}
