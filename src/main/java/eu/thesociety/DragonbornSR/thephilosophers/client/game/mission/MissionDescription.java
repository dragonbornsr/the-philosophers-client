package eu.thesociety.DragonbornSR.thephilosophers.client.game.mission;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class MissionDescription {
	public String mission_name;
	public String[] authors;
	public String version;
	public String description;
	
	public static MissionDescription load(String missionLocation, boolean isSteamWorkshopContent) {
		Gson gson = new Gson();
		JsonReader reader;
		try {
			reader = new JsonReader(new FileReader(missionLocation));
			MissionDescription md = gson.fromJson(reader, MissionDescription.class);
			return md;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}