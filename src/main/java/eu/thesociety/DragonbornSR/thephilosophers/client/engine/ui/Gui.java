package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;

public abstract class Gui extends DrawableGameObject {

	public List<Button> listOfButtons = new ArrayList<Button>();
	public List<GuiTextBox> listOfGuiTextBoxes = new ArrayList<GuiTextBox>();
	/**
	 * Boolean focus defines, if GUI is on top of others. This Must be set by
	 * GUI handler
	 */
	protected static Gui focused = null;

	public int height, width, posX, posY, posZ;
	protected int defaultPosZ = 0;
	
	private Texture background;
	private float backgroundTransparency = 1f;

	public boolean getIsFocused() {
		return this.equals(focused);
	}

	public void setFocused(boolean focus) {
		focused = this;
	}

	/**
	 * set GUI size
	 * 
	 * @param height
	 * @param width
	 */
	public void setSize(int width, int height) {
		this.height = height;
		this.width = width;
	}

	/**
	 * Set GUI position
	 * 
	 * @param x
	 * @param y
	 */
	public void setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
		this.posZ = this.defaultPosZ;
	}

	/**
	 * Allows GUI to be drawn also with Z scale.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	public void setPosition(int x, int y, int z) {
		this.posX = x;
		this.posY = y;
		this.posZ = z;
	}
	
	/**
	 * @param background the background to set
	 */
	public void setBackground(Texture background) {
		this.background = background;
	}
	
	/**
	 * Method that is used by buttons to forward that they were pressed.
	 * @param button Button
	 */
	public abstract void buttonPressed(Button button);
	
	/**
	 * Method that is used by buttons to forward that they were released.
	 * 
	 * @param button
	 */
	public abstract void buttonReleased(Button button);
	
	/**
	 * Method that is used by text boxes to forward when they were changed
	 * @param textBox GuiTextBox
	 */
	public abstract void textBoxChanged(GuiTextBox textBox);
	
	/**
	 * Method that is used by text boxes to forward when enter was pressed while they were active
	 * @param textBox GuiTextBox
	 */
	public abstract void textBoxEntered(GuiTextBox textBox);

	@Override
	public void draw(float delta) {
		GL11.glPushMatrix();
		background.bind();
		GL11.glColor4f(1f, 1f, 1f, backgroundTransparency);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex2f(posX, posY);
		GL11.glTexCoord2f((float) width / background.getWidth(), 0.0f);
		GL11.glVertex2f(posX + width, posY);
		GL11.glTexCoord2f((float) width / background.getWidth(), (float) height / background.getHeight());
		GL11.glVertex2f(posX + width, posY + height);
		GL11.glTexCoord2f(0, (float) height / background.getHeight());
		GL11.glVertex2f(posX, posY + height);
		GL11.glEnd();
		GL11.glColor4f(1f, 1f, 1f, 1f);
		GL11.glPopMatrix();
	}

	@Override
	public void update(float delta) {
		
	}

	@Override
	public void finalize() {
	
	}
	
	public void setBackgroundTransparency(float value) {
		this.backgroundTransparency = value;
	}
	
	public int getWidth() { return this.width; }
	public int getHeight() { return this.height; }
	public int getX() { return this.posX; }
	public int getY() { return this.posY; }
}