package eu.thesociety.DragonbornSR.thephilosophers.client.engine;

import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryUtil;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;

import java.io.File;

/**
 * @author Oliver
 *
 */
public class Display {

	static {
		System.setProperty("org.lwjgl.librarypath", new File("natives").getAbsolutePath());
	}

	private GLFWErrorCallback errorCallback;
	private GLFWKeyCallback keyCallback;
	private GLFWWindowSizeCallback resizeCallback;

	private long window;
	private boolean initDone;

	public static int WIDTH = 1280;
	public static int HEIGHT = 720;
	
	private static int WIDTH_MIN = 1280;
	private static int HEIGHT_MIN = 720;

	public void run() {
		init();
		initDone = true;

		GL.createCapabilities();
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, WIDTH, 0, HEIGHT, 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}

	public void destroy() {
		GLFW.glfwDestroyWindow(window);
		keyCallback.release();
		resizeCallback.release();
		GLFW.glfwTerminate();
		errorCallback.release();
	}

	private void init() {
		GLFW.glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));

		if (GLFW.glfwInit() != GLFW.GLFW_TRUE)
			throw new IllegalStateException("Unable to initialize GLFW");

		GLFW.glfwDefaultWindowHints();
		GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
		GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE);

		window = GLFW.glfwCreateWindow(WIDTH, HEIGHT, "The Philosophers", MemoryUtil.NULL, MemoryUtil.NULL);
		if (window == MemoryUtil.NULL)
			throw new RuntimeException("Failed to create the GLFW window");
		

		GLFW.glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_RELEASE)
					GLFW.glfwSetWindowShouldClose(window, GLFW.GLFW_TRUE); 
			}
		});

		GLFW.glfwSetWindowSizeCallback(window, resizeCallback = new GLFWWindowSizeCallback() {
			@Override
			public void invoke(long window, int width, int height) {
				boolean badSize = false;
				if (width >= WIDTH_MIN)
					WIDTH = width;
				else {
					badSize = true;
					WIDTH = WIDTH_MIN;
				}
				if (height >= HEIGHT_MIN)
					HEIGHT = height;
				else { 
					badSize = true;
					HEIGHT = HEIGHT_MIN;
				}
				if (badSize) GLFW.glfwSetWindowSize(window, (width < WIDTH_MIN)?WIDTH_MIN:WIDTH, (height < HEIGHT_MIN)?HEIGHT_MIN:HEIGHT);
				
				for (DrawableGameObject drawableGameObject : ThePhilosophersClient.listOfDrawableGameObjects) {
					drawableGameObject.onDisplayResized(WIDTH, HEIGHT);
				}

				if (initDone) {
					GL11.glViewport(0, 0, WIDTH, HEIGHT);
					GL11.glMatrixMode(GL11.GL_PROJECTION);
					GL11.glLoadIdentity();
					GL11.glOrtho(0, WIDTH, 0, HEIGHT, 1, -1);
				}
			}
		});

		GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		GLFW.glfwSetWindowPos(window, (vidmode.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 2);
		GLFW.glfwMakeContextCurrent(window);
		GLFW.glfwSwapInterval(1);
		GLFW.glfwShowWindow(window);
	}

	public void render() {
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		for (DrawableGameObject dgo : ThePhilosophersClient.listOfDrawableGameObjects) {
			if (dgo.isEnabled() && dgo.isVisible())
				dgo.draw(ThePhilosophersClient.th.getDelta());
		}

		GLFW.glfwSwapBuffers(window);
		GLFW.glfwPollEvents();
	}

	public boolean isCloseRequested() {
		return GLFW.glfwWindowShouldClose(window) == 1;
	}
	
	public long getDisplayId() {
		return window;
	}
}
