package eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;

public abstract class CoreObject {

	public abstract void update(float delta);

	protected void register() {
		ThePhilosophersClient.listOfCoreObjects.add(this);
	}
}