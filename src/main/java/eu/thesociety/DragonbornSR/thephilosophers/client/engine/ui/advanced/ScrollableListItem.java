package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.TextureTools;

public abstract class ScrollableListItem extends DrawableGameObject implements IMouseEvent {
	
	protected ScrollableList parentScrollableList;
	protected int width, height;
	protected boolean hover = false;
	protected int texOffX = 0, texOffY = 0;
	protected Texture texture;
	
	protected Vector2f c1 = new Vector2f(0, 0), // Corner 1 is upper left,
			c2 = new Vector2f(0, 0), // following ones are clockwise.
			c3 = new Vector2f(0, 0), c4 = new Vector2f(0, 0);
	
	public ScrollableListItem(ScrollableList parentScrollableList) {
		this.parentScrollableList = parentScrollableList;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void update(float delta) {
		if (!hover && (ThePhilosophersClient.hovering == this || this.parentScrollableList.getSelectedItem() == this)) {
			this.texOffY += this.height;
			this.hover = true;
			this.updateTextureCorners();
		} else if (hover && ThePhilosophersClient.hovering != this && this.parentScrollableList.getSelectedItem() != this) {
			this.texOffY -= this.height;
			this.hover = false;
			this.updateTextureCorners();
		}
	}
	

	@Override
	public void mouseMoved(double x, double y) {
		ThePhilosophersClient.hovering = this;
		
	}
	
	public abstract void draw(int pos, int x, int y);
	
	public abstract void onDisplayResized(int width, int height);
	
	/**
	 * Update texture corner positions to allow texture off-setting.
	 */
	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY + height);
		c2 = TextureTools.getTextureCorner(this.texture, texOffX + width, texOffY + height);
		c3 = TextureTools.getTextureCorner(this.texture, texOffX + width, texOffY);
		c4 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY);
	}
}
