package eu.thesociety.DragonbornSR.thephilosophers.client.game.mission;

import java.io.IOException;
import java.util.ArrayList;

import com.igormaznitsa.prol.data.Term;
import com.igormaznitsa.prol.logic.Goal;
import com.igormaznitsa.prol.logic.ProlContext;
import com.igormaznitsa.prol.utils.Utils;

import eu.thesociety.DragonbornSR.thephilosophers.client.game.Player;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.PlayerAction;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.ArraysUtil;

/**
 * Class that communicates with polog and returns us needed information.
 * @author Oliver
 *
 */
public class Mission {
	
	private ProlContext prolContext;
	private String missionLocation;
	private boolean isSteamWorkshopContent;
	private MissionDescription missionDescription;
	private String authorsString;
	
	private static final int MAX_PL_RESULTS = 50;
	
	public Mission(String missionLocation, ProlContext context, boolean isSteamWorkshopContent) {
		this.setMissionLocation(missionLocation);
		this.prolContext = context;
		this.isSteamWorkshopContent = isSteamWorkshopContent;
		this.missionDescription = MissionDescription.load(missionLocation + "/mission.json", isSteamWorkshopContent);
		this.authorsString = ArraysUtil.stringArraySeparatedBy(this.getMissionAuthors(), ", ");
	}
	
	public String getMissionName() {
		return this.missionDescription.mission_name;
	}
	
	public String getMissionVersion() {
		return this.missionDescription.version;
	}
	
	public String[] getMissionAuthors() {
		return this.missionDescription.authors;
	}
	
	public String getMissionAuthorsAsString() {
		return this.authorsString;
	}
	
	
	public ArrayList<String> getMissionPlayers() {
		ArrayList<String[]> query = query("player", new String[] {"Player"});
		ArrayList<String> result = new ArrayList<String>();
		for (String[] pr : query) {
			result.add(pr[0]);
		}
		return result;
	}
	
	public ArrayList<String> getDeadPlayers() {
		ArrayList<String[]> query = query("dead", new String[] {"Player"});
		ArrayList<String> result = new ArrayList<String>();
		for (String[] pr : query) {
			result.add(pr[0]);
		}
		return result;
	}
	
	public boolean isDead(Player p) {
		ArrayList<String[]> query = query("dead", new String[] {p.getQuotedPlayerName()});
		if (query.size() > 0) return true;
		return false;
	}
	
	public boolean isMale(Player p) {
		ArrayList<String[]> query = query("male", new String[] {p.getQuotedPlayerName()});
		if (query.size() > 0) return true;
		return false;
	}
	
	public ArrayList<String> getProffessions(Player p) {
		ArrayList<String[]> query = query("proffession", new String[] {p.getQuotedPlayerName(), "Proffession"});
		ArrayList<String> result = new ArrayList<String>();
		for (String[] pr : query) {
			result.add(pr[1]);
		}
		return result;
	}
	
	public ArrayList<PlayerAction> getPlayerActions(Player p) {
		ArrayList<String[]> query = query("action", new String[] {
				p.getQuotedPlayerName(), 
				"Group", 
				"ParentGroup", 
				"ActionName", 
				"TargetList", 
				"Text", 
				"Image", 
				"Sound", 
				"Duration", 
				"false"});
		ArrayList<PlayerAction> result = new ArrayList<PlayerAction>();
		for (String[] pr : query) {
			result.add(new PlayerAction(p, pr[1], pr[2], pr[3], pr[4].substring(1, pr[4].length() - 1).split(","), pr[5], pr[6], pr[7], Float.parseFloat(pr[8])));
		}
		return result;
	}
	
	private String[] getValuesFromTerm(Term t) {
		String str = Utils.getStringFromElement(t);
		str = str.substring(str.indexOf("(") + 1, str.length() - 1);
		return str.split(",");
	}
	
	public ArrayList<String[]> query(String question, String[] param) {
		ArrayList<String[]> queryResult = new ArrayList<String[]>();
		try {
			final Goal goal = new Goal(question + "(" + String.join(",", param) + ").", prolContext);
			
			int count = 0;
			while (count < MAX_PL_RESULTS) {
                final Term result = goal.solve();
                if (result == null) break;
                queryResult.add(getValuesFromTerm(result));
                count++;
            }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return queryResult;
	}

	/**
	 * @return the name
	 */
	public String getMissionLocation() {
		return missionLocation;
	}

	/**
	 * @param name the name to set
	 */
	public void setMissionLocation(String missionLocation) {
		this.missionLocation = missionLocation;
	}

	/**
	 * @return the isSteamWorkshopContent
	 */
	public boolean isSteamWorkshopContent() {
		return isSteamWorkshopContent;
	}
}
