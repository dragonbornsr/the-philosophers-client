package eu.thesociety.DragonbornSR.thephilosophers.client.game;

import java.util.HashMap;

import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamMatchMakingKeyValuePair;

public class Lobby  {
	
	public static final String KEY_GAMEKEY = "GameKey";
	public static final String KEY_HOST_NAME = "SteamLobbyHostName";
	public static final String KEY_HOST_ID = "SteamLobbyHostId";
	public static final String KEY_MAX_MEMBERS = "SteamLobbyMaxMembers";
	public static final String KEY_MISSION_NAME = "MissionName";
	public static final String KEY_MISSION_LOCATION = "MissionLocation";

	private SteamID steamid;
	
	private HashMap<Integer,Player> players = new HashMap<Integer, Player>();
	
	public Lobby(SteamID steamid) {
		this.steamid = steamid;
		this.updateLobbyUserlist();
	}

	public void updateLobbyUserlist() {
		int members = getLobbyMemberCount();
		for (int mid = 0; mid < members; mid++) {
			SteamID member = Matchmaking.instance().getSteamMatchmaking().getLobbyMemberByIndex(steamid, mid);
			if (member.isValid())
				players.put(member.getAccountID(), new Player(member));
		}
	}

	public SteamID getSteamID() {
		return steamid;
	}

	public boolean isValid() {
		return true;
		//return getEncryptedGameKey().equals(Crypto.MD5(ThePhilosophersClient.gamekey));
	}
	
	public String getLobbyHostName() {
		return Matchmaking.instance().getSteamMatchmaking().getLobbyData(steamid, KEY_HOST_NAME);
	}
	
	public String getMissionName() {
		return Matchmaking.instance().getSteamMatchmaking().getLobbyData(steamid, KEY_MISSION_NAME);
	}
	
	public String getLobbyHostId() {
		return Matchmaking.instance().getSteamMatchmaking().getLobbyData(steamid, KEY_HOST_ID);
	}
	
	public int getLobbyMemberCount() {
		return Matchmaking.instance().getSteamMatchmaking().getNumLobbyMembers(steamid);
	}
	
	public int getLobbyMaxMembers() {
		String result = Matchmaking.instance().getSteamMatchmaking().getLobbyData(steamid, KEY_MAX_MEMBERS);
		return (result.length() > 0) ? Integer.parseInt(result) : 0;
	}
	
	public String getEncryptedGameKey() {
		return Matchmaking.instance().getSteamMatchmaking().getLobbyData(steamid, KEY_GAMEKEY);
	}
	
	public void updateLobbyData() {
		int dataCount = Matchmaking.instance().getSteamMatchmaking().getLobbyDataCount(steamid);
		for (int i = 0; i < dataCount; i++) {
			SteamMatchMakingKeyValuePair pair = new SteamMatchMakingKeyValuePair();
			Matchmaking.instance().getSteamMatchmaking().getLobbyDataByIndex(steamid, i, pair);
			Matchmaking.instance().getSteamMatchmaking().setLobbyData(steamid, pair.getKey(), pair.getValue());
		}
	}
	
	public static void updateLobbyData(SteamID steamid) {
		int dataCount = Matchmaking.instance().getSteamMatchmaking().getLobbyDataCount(steamid);
		for (int i = 0; i < dataCount; i++) {
			SteamMatchMakingKeyValuePair pair = new SteamMatchMakingKeyValuePair();
			Matchmaking.instance().getSteamMatchmaking().getLobbyDataByIndex(steamid, i, pair);
			Matchmaking.instance().getSteamMatchmaking().setLobbyData(steamid, pair.getKey(), pair.getValue());
		}
	}
	
	public void removePlayer(SteamID steamID) {
		players.remove(steamID.getAccountID());
	}
	
	public void addPlayer(SteamID steamID) {
		players.put(steamID.getAccountID(), new Player(steamID));
	}
	
	public Player getPlayer(SteamID steamID) {
		return players.get(steamID.getAccountID());
	}

	public HashMap<Integer, Player> getPlayers() {
		return this.players;
	}
}
