package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

import java.awt.Color;

public class TextButton extends Button {
	
	private Color defaultTextColor;
	private Color focusedTextColor;
	
	private TrueTypeFont ttf;
	private String text = "";
	
	int textXoffset = 0;
	int textYoffset = 0;
	
	private String fontname;
	private int fontsize;

	public TextButton(Gui parent) {
		super(parent);
	}
	
	@Override
	public void draw(float delta) {
		super.draw(delta);
		if (this.isVisible()) {
			if (this.hover)
				ttf.draw(text, parent.posX + this.posX + textXoffset, parent.posY + this.posY + textYoffset, focusedTextColor);
			else 
				ttf.draw(text, parent.posX + this.posX + textXoffset, parent.posY + this.posY + textYoffset, defaultTextColor);
		}
	}

	/**
	 * @return the defaultTextColor
	 */
	public Color getDefaultTextColor() {
		return defaultTextColor;
	}

	/**
	 * @param defaultTextColor the defaultTextColor to set
	 */
	public TextButton setDefaultTextColor(Color defaultTextColor) {
		this.defaultTextColor = defaultTextColor;
		return this;
	}

	/**
	 * @return the focusedTextColor
	 */
	public Color getFocusedTextColor() {
		return focusedTextColor;
	}

	/**
	 * @param focusedTextColor the focusedTextColor to set
	 */
	public TextButton setFocusedTextColor(Color focusedTextColor) {
		this.focusedTextColor = focusedTextColor;
		return this;
	}
	
	private void calculateTextPosition() {
		int textWidth = ttf.getTextWidth(text);
		int textHeight = ttf.getLineHeight();
		textXoffset = this.width / 2 - (textWidth + 4) / 2;
		textYoffset = this.height / 2 - textHeight / 4;
	}
	
	@Override
	public void onDisplayResized(int width, int height) {
		super.onDisplayResized(width, height);
		//this.calculateTextPosition();
	}
	
	public void refreshFonts() {
		ttf = new TrueTypeFont(fontname, fontsize);
		this.calculateTextPosition();
	}

	/**
	 * @return the fontsize
	 */
	public int getFontsize() {
		return fontsize;
	}

	/**
	 * @param fontsize the fontsize to set
	 */
	public TextButton setFontsize(int fontsize) {
		this.fontsize = fontsize;
		this.refreshFonts();
		return this;
	}

	/**
	 * @return the fontname
	 */
	public String getFontname() {
		return fontname;
	}

	/**
	 * @param fontname the fontname to set
	 */
	public TextButton setFontname(String fontname) {
		this.fontname = fontname;
		this.refreshFonts();
		return this;
	}
	
	public TextButton setText(String text) {
		this.text = text;
		this.calculateTextPosition();
		return this;
	}

}
