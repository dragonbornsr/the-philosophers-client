package eu.thesociety.DragonbornSR.thephilosophers.client.gui.fx;

import java.util.ArrayList;
import java.util.Random;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.fx.particles.ParticlePath;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.fx.particles.ParticleSystem;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.TextureTools;

public class BackgroundParticleSystem extends ParticleSystem {
	
	private ArrayList<ArrayList<Vector2f>> textureCorners = new ArrayList<ArrayList<Vector2f>>();
	
	private int w = 105, h = 105;
	private int offsetX = 26, offsetY = 28;
	private byte[] seeds = {10, 9, 6, 5};
	private Random rand = new Random();
	public BackgroundParticleSystem(int priority) {
		this.texture = ResourceManager.getTexture("resources/bg-dots.png");
		this.generateTextureCorners();
		
		// diagonal
		paths.add(new ParticlePath() {
			@Override // seed (right, left, up, down)
			public Vector2f getParticleLocation(float[] particle, float delta) {
				if (delta > 10) delta = 1f;
				byte seed = (byte)particle[PARTICLE.SEED.getValue()];
				return new Vector2f(
						particle[PARTICLE.X.getValue()] + delta * getBit(3, seed) * particle[PARTICLE.SPEEDX.getValue()] - delta * getBit(2, seed) * particle[PARTICLE.SPEEDX.getValue()],
						particle[PARTICLE.Y.getValue()] + delta * getBit(1, seed) * particle[PARTICLE.SPEEDY.getValue()] - delta * getBit(0, seed) * particle[PARTICLE.SPEEDY.getValue()]);
			}
		});
		// parabol X telge pidi
		paths.add(new ParticlePath() {
			@Override // seed (right, left, up, down)
			public Vector2f getParticleLocation(float[] particle, float delta) {
				if (delta > 10) delta = 1f;
				byte seed = (byte)particle[PARTICLE.SEED.getValue()];
				float x2 =  (particle[PARTICLE.SUMDELTA.getValue()]*particle[PARTICLE.SUMDELTA.getValue()] / 1000);
				return new Vector2f(
						particle[PARTICLE.X.getValue()] + delta * getBit(3, seed) * particle[PARTICLE.SPEEDX.getValue()] - delta * getBit(2, seed) * particle[PARTICLE.SPEEDX.getValue()],
						getBit(1, seed) * x2 * particle[PARTICLE.SPEEDY.getValue()] - getBit(0, seed) * x2 * particle[PARTICLE.SPEEDY.getValue()]);
			}
		});

		this.register(h);
	}

	private void generateTextureCorners() {
		for (int y = 0; y < 6; y++) {
			for (int x = 0; x < 8; x++) {
				ArrayList<Vector2f> group = new ArrayList<Vector2f>();
				group.add(TextureTools.getTextureCorner(this.texture, x*w+x*offsetX, y*h+y*offsetY + h));
				group.add(TextureTools.getTextureCorner(this.texture, x*w+x*offsetX + w, y*h+y*offsetY + h));
				group.add(TextureTools.getTextureCorner(this.texture, x*w+x*offsetX + w, y*h+y*offsetY));
				group.add(TextureTools.getTextureCorner(this.texture, x*w+x*offsetX, y*h+y*offsetY));	
				textureCorners.add(group);
			}
		}
	}
	
	public void spawnRandomParticleAtPoint(int x, int y) {
		int textureCorner = this.getRandomTexture();
		particles.add(new float[] {x, y, 105, 105, getRandomSpeed(), getRandomSpeed(), this.getRandomPath(), 0, 
				textureCorners.get(textureCorner).get(0).getX(),
				textureCorners.get(textureCorner).get(0).getY(),
				textureCorners.get(textureCorner).get(1).getX(),
				textureCorners.get(textureCorner).get(1).getY(),
				textureCorners.get(textureCorner).get(2).getX(),
				textureCorners.get(textureCorner).get(2).getY(),
				textureCorners.get(textureCorner).get(3).getX(),
				textureCorners.get(textureCorner).get(3).getY(),
				(float)this.getRandomSeed(),
				getRandomScale(),getRandomOpacity(), 0});
		textureCorner = this.getRandomTexture();
	}
	
	protected float getRandomScale() {
		return rand.nextFloat() * (0.7f - 0.3f) + 0.3f;
	}
	
	protected float getRandomOpacity() {
		return rand.nextFloat() * (0.5f - 0.025f) + 0.1f;
	}
	
	protected float getRandomSpeed() {
		return rand.nextFloat() * (0.2f - 0.05f) + 0.05f;
	}
	
	protected int getRandomTexture() {
		return 0 + (int)(Math.random() * (textureCorners.size() - 1));
	}
	
	protected byte getRandomSeed() {
		return seeds[0 + (int)(Math.random() * (seeds.length - 1))];
	}
}
