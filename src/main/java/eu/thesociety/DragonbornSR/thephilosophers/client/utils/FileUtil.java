package eu.thesociety.DragonbornSR.thephilosophers.client.utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileUtil {
	
	public static String fileToString(String file) {
		try {
			return FileUtils.readFileToString(new File(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
