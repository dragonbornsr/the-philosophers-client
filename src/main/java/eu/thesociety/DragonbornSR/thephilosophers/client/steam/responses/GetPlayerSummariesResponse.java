package eu.thesociety.DragonbornSR.thephilosophers.client.steam.responses;

import java.util.ArrayList;

public class GetPlayerSummariesResponse {
	
	public GetPlayerSummariesResponseContent response;
	
	public class GetPlayerSummariesResponseContent {
		public ArrayList<PlayerInfo> players;
	}
	
	public class PlayerInfo {
		public String steamid;
		public int communityvisibilitystate;
		public int profilestate;
		public String personaname;
		public int lastlogoff;
		public String profileurl;
		public String avatar;
		public String avatarmedium;
		public String avatarfull;
		public int personastate;
	}
}
