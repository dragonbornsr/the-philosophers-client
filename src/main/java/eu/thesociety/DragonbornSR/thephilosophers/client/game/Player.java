package eu.thesociety.DragonbornSR.thephilosophers.client.game;

import com.codedisaster.steamworks.SteamHTTP.API;
import com.codedisaster.steamworks.SteamHTTP.HTTPStatusCode;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IAsyncImageCallback;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.AsyncImageLoader;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.steam.responses.GetPlayerSummariesResponse;
import eu.thesociety.DragonbornSR.thephilosophers.client.steam.responses.GetPlayerSummariesResponse.PlayerInfo;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import com.codedisaster.steamworks.SteamException;
import com.codedisaster.steamworks.SteamHTTP;
import com.codedisaster.steamworks.SteamHTTPCallback;
import com.codedisaster.steamworks.SteamHTTPRequestHandle;
import com.codedisaster.steamworks.SteamID;
import com.google.gson.Gson;

public class Player implements SteamHTTPCallback, IAsyncImageCallback {
	
	private SteamID steamID;
	private SteamHTTP steamHTTP;
	private PlayerInfo playerInfo;
	private boolean isLoaded = false;
	private AsyncImageLoader imageRequest;
	private boolean imageWaiting = false;
	
	private final Charset messageCharset = Charset.forName("UTF-8");
	public Texture profileimage;
	public BufferedImage imageData;
	
	public Player(SteamID steamID) {
		this.steamID = steamID;
		this.profileimage = ResourceManager.getTexture("default_playericon_256x256");
		steamHTTP = new SteamHTTP(this, API.Client);
		this.requestPlayerInfo();
	}

	public SteamID getSteamID() {
		return steamID;
	}
	
	public String getPlayerName() {
		return playerInfo.personaname;
	}
	
	public String getQuotedPlayerName() {
		return "'" + playerInfo.personaname + "'";
	}
	
	public void setPlayerName(String name) {
		this.playerInfo.personaname = name;
	}
	
	@SuppressWarnings("deprecation")
	private void requestPlayerInfo() {
		SteamHTTPRequestHandle request = steamHTTP.createHTTPRequest(SteamHTTP.HTTPMethod.GET, 
				"https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/" );
		
		steamHTTP.setHTTPRequestGetOrPostParameter(request, "steamids", ""+this.getSteamID().getNativeHandle());
		steamHTTP.setHTTPRequestGetOrPostParameter(request, "key", ThePhilosophersClient.apikey);
		steamHTTP.setHTTPRequestGetOrPostParameter(request, "format", "json");
		steamHTTP.sendHTTPRequest(request);
	}

	@Override
	public void onHTTPRequestCompleted(SteamHTTPRequestHandle request, long contextValue, boolean requestSuccessful,
			HTTPStatusCode statusCode, int bodySize) {
		ByteBuffer data = ByteBuffer.allocateDirect(bodySize);
		try {
			steamHTTP.getHTTPResponseBodyData(request, data);
			byte[] bytes = new byte[bodySize];
			data.get(bytes);
			Gson g = new Gson();
			GetPlayerSummariesResponse responseObject = g.fromJson(new String(bytes, messageCharset), GetPlayerSummariesResponse.class);
			this.playerInfo = responseObject.response.players.get(0);
			imageRequest = new AsyncImageLoader(this.playerInfo.avatarfull, this);
		} catch (SteamException e) {
			e.printStackTrace();
		}
		steamHTTP.releaseHTTPRequest(request);
	}

	@Override
	public void onHTTPRequestHeadersReceived(SteamHTTPRequestHandle request, long contextValue) {
		
	}

	@Override
	public void onHTTPRequestDataReceived(SteamHTTPRequestHandle request, long contextValue, int offset,
			int bytesReceived) {

	}

	/**
	 * @return the isLoaded
	 */
	public boolean isLoaded() {
		return isLoaded;
	}

	/**
	 * @param isLoaded the isLoaded to set
	 */
	public void setLoaded(boolean isLoaded) {
		this.isLoaded = isLoaded;
	}

	@Override
	public void onImageReceived(String url, BufferedImage bi) {
		this.imageData = bi;
		this.imageWaiting = true;
		this.imageRequest = null;
	}

	public boolean isImageWaiting() {
		return this.imageWaiting;
	}

}
