package eu.thesociety.DragonbornSR.thephilosophers.client.engine;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;

public class TickHandler {

	protected long now, lastTime = System.nanoTime();
	protected int frames = 0;
	protected double process = 0.0;
	protected long frameTime = System.currentTimeMillis();
	protected double NSperTick = 1000000000.0 / 60.0;
	public int tickSpeed = 60;
	protected int TPS = 60;
	
	protected long handleTime, updateTime, drawTime;

	public TickHandler() {
		lastTime = System.nanoTime();
	}

	/**
	 * set Maximum FPS or TPS (depends on context)
	 * 
	 * @param
	 */
	public void setMaxTPS(int tickSpeed) {
		this.tickSpeed = tickSpeed;
		TPS = tickSpeed;
		NSperTick = 1000000000.0 / tickSpeed;
	}

	/**
	 * 
	 * @return current FPS or TPS
	 */
	public int getLastTPS() {
		return TPS;
	}

	/**
	 * Delta is a speed constant. 60 frames per second will be constant 1. if
	 * FPS is 120, every movement per frame will be multiplied with 0.5 to
	 * achieve same object speed.
	 * 
	 * @return
	 */
	public float getDelta() {
		return (float) 60 / TPS;
	}

	public void handle(ThePhilosophersClient g) throws InterruptedException {
		now = System.nanoTime();
		process += (now - lastTime) / NSperTick;
		lastTime = now;
		boolean ticked = false;
		while (process >= 1) {
			process -= 1;
			ticked = true;
		}
		if (ticked) {
			long t = System.nanoTime();
			g.updateLoop();
			updateTime = System.nanoTime() - t;
			t = System.nanoTime();
			g.drawLoop();
			drawTime = System.nanoTime() - t;
			frames++;
		} else {
			Thread.sleep(0, 100);

		}

		if (frameTime <= System.currentTimeMillis() - 1000) {
			TPS = frames;
			frameTime += 1000;
			frames = 0;
		}
		handleTime = System.nanoTime() - lastTime;
	}
	
	public long getLastUpdateTime() {
		return updateTime;
	}
	public long getLastDrawTime() {
		return drawTime;
	}
	public long getLastHandleTime() {
		return handleTime;
	}
	public long getLastIdleTime() {
		return handleTime - updateTime - drawTime;
	}
}