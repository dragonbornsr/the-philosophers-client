package eu.thesociety.DragonbornSR.thephilosophers.client.utils;

public class Keyboard {
	
	private static final int GLFW_MOD_SHIFT   = 0x0001;
	private static final int GLFW_MOD_CONTROL = 0x0002;
	private static final int GLFW_MOD_ALT     = 0x0004;
	private static final int GLFW_MOD_SUPER   = 0x0008;
	
	private static int TRUE = 1;
	
	public static boolean isShift(int mods) {
		return (mods & GLFW_MOD_SHIFT) == TRUE;
	}
	
	public static boolean isAlt(int mods) {
		return (mods & GLFW_MOD_ALT) == TRUE;
	}
	
	public static boolean isControl(int mods) {
		return (mods & GLFW_MOD_CONTROL) == TRUE;
	}
	
	public static boolean isSuper(int mods) {
		return (mods & GLFW_MOD_SUPER) == TRUE;
	}
	
	public static char getKey(int key, int scancode, int mods) {
		return 0;
	}

}
