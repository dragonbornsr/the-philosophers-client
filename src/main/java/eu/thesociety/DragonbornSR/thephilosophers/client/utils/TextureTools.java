package eu.thesociety.DragonbornSR.thephilosophers.client.utils;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;

public class TextureTools {

	/**
	 * 
	 * @param t (Texture)
	 * @param x (int)
	 * @param y (int)
	 * @return Vector2f to represent a specific pixel on Texture.
	 */
	public static Vector2f getTextureCorner(Texture t, int x, int y) {
		return new Vector2f(((float) x / t.getWidth()),
				((float) y / t.getHeight()));
	}
}