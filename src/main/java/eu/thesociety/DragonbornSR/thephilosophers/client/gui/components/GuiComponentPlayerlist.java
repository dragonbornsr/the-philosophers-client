package eu.thesociety.DragonbornSR.thephilosophers.client.gui.components;

import java.awt.Color;
import java.util.Collection;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Button;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Gui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.GuiTextBox;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.TrueTypeFont;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Lobby;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Player;

public class GuiComponentPlayerlist extends Gui {
	
	private Gui parent;
	private Lobby lobby;
	private TrueTypeFont font;
	private TrueTypeFont font_small;
	private TrueTypeFont font_tiny;
	private int defaut_fontSize = 24;
	int imageSide = 64;
	
	public GuiComponentPlayerlist(int x, int y, int width, int height, Gui parent, Lobby lobby) {
		this.setPosition(x, y);
		this.setSize(width, height);
		this.parent = parent;
		this.lobby = lobby;
		this.setBackground(ResourceManager.getTexture("white"));
		this.setBackgroundTransparency(0.0f);
		this.register(parent.priority + 1);
		font = new TrueTypeFont("resources/fonts/DIOGENES.ttf", defaut_fontSize);
		font_small = new TrueTypeFont("resources/fonts/DIOGENES.ttf", defaut_fontSize - 3);
		font_tiny = new TrueTypeFont("resources/fonts/DIOGENES.ttf", defaut_fontSize - 6);
	}
	
	@Override
	public void update(float delta) {
		
	}
	
	@Override
	public boolean isEnabled() {
		return parent.isEnabled();
	}
	
	@Override
	public boolean isVisible() {
		return parent.isVisible();
	}
	
	@Override
	public void draw(float delta) {
		super.draw(delta);
		Collection<Player> players = lobby.getPlayers().values();
		int cr = 0;
		int cc = 0;
		int offset_x = (this.width - (5*imageSide)) / 5;
		int offset_y = font.getFontHeight() + 3;
		for (Player player : players) {	
			if (player.isLoaded()) {
				if (cc % 5 == 0) { cr++; cc = 0; }
				if (cr > 4) return;

				int modA = (int) (cc * imageSide + (cc) * offset_x + 0.5f * offset_x + this.posX);
				int modB = this.posY - ((cr) * imageSide + (cr - 1) * offset_y - this.height + (cr - 1) * 20);
				
				GL11.glPushMatrix();
				player.profileimage.bind();
				GL11.glColor4f(1f, 1f, 1f, 1f);
				GL11.glBegin(GL11.GL_QUADS);
				
				GL11.glTexCoord2f(0f, 0f);
				GL11.glVertex2f(modA, modB + imageSide);
				
				GL11.glTexCoord2f(1f, 0.0f);
				GL11.glVertex2f(modA + imageSide, modB + imageSide);
				
				GL11.glTexCoord2f(1f, 1f);
				GL11.glVertex2f(modA + imageSide, modB);
				
				GL11.glTexCoord2f(0, 1f);
				GL11.glVertex2f(modA, modB);
				
				GL11.glEnd();
				GL11.glColor4f(1f, 1f, 1f, 1f);
				GL11.glPopMatrix();
				cc++;
				int playernameWidth = font.getTextWidth(player.getPlayerName());
				if (playernameWidth <= imageSide + offset_x)
					font.draw(player.getPlayerName(), modA + (int)(0.5*imageSide) - playernameWidth / 2, modB - font.getFontHeight(), Color.CYAN);
				else {
					int playernameWidth_small = font_small.getTextWidth(player.getPlayerName());
					if (playernameWidth <= imageSide + offset_x) {
						font_small.draw(player.getPlayerName(), modA + (int)(0.5*imageSide) - playernameWidth_small / 2, modB - font.getFontHeight(), Color.CYAN);
					} else {
						int playernameWidth_tiny = font_tiny.getTextWidth(player.getPlayerName());
						font_tiny.draw(player.getPlayerName(), modA + (int)(0.5*imageSide) - playernameWidth_tiny / 2, modB - font.getFontHeight(), Color.CYAN);
					}
				}
			} else {
				if (player.isImageWaiting()) {
					if (player.imageData != null) {
						player.profileimage = new Texture(player.imageData);
						player.setLoaded(true);
					}
				}
			}
		}
	}

	@Override
	public void buttonPressed(Button button) {

	}

	@Override
	public void buttonReleased(Button button) {

	}

	@Override
	public void textBoxChanged(GuiTextBox textBox) {

	}

	@Override
	public void textBoxEntered(GuiTextBox textBox) {

	}

	@Override
	public void onDisplayResized(int width, int height) {

	}

}
