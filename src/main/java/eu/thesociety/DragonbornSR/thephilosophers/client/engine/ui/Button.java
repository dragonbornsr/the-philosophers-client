package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

import org.lwjgl.opengl.GL11;
import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.TextureTools;

public class Button extends DrawableGameObject implements IMouseEvent {

	protected Gui parent;
	protected int posX = 0, posY = 0;
	protected int width = 0, height = 0;
	public int texOffX = 0, texOffY = 0;
	protected Texture texture = null;

	/**
	 * parameters that describe texture corners.
	 */
	protected Vector2f c1 = new Vector2f(0, 0), // Corner 1 is upper left,
			c2 = new Vector2f(0, 0), // following ones are clockwise.
			c3 = new Vector2f(0, 0), c4 = new Vector2f(0, 0);

	protected boolean hover = false;

	/**
	 * Initialize gui button. REMEMBER, please also set size and position for
	 * the button!!!
	 * 
	 * @param parent
	 * @param buttonID
	 */
	public Button(Gui parent) {
		this.parent = parent;
		parent.listOfButtons.add(this);
		register(parent.priority + 1);
	}

	@Override
	public void update(float delta) {
		if (!hover && ThePhilosophersClient.hovering == this) {
			this.setXOffset(texOffX + this.width);
			this.hover = true;
			this.updateTextureCorners();
		} else if (hover && ThePhilosophersClient.hovering != this) {
			this.setXOffset(texOffX - this.width);
			this.hover = false;
			this.updateTextureCorners();
		} 

	}

	@Override
	public void draw(float delta) {
		GL11.glPushMatrix();
		texture.bind();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(c1.getX(), c1.getY());
		GL11.glVertex2f(posX + parent.posX, posY + parent.posY);
		GL11.glTexCoord2f(c2.getX(), c2.getY());
		GL11.glVertex2f(posX + parent.posX + width, posY + parent.posY);
		GL11.glTexCoord2f(c3.getX(), c3.getY());
		GL11.glVertex2f(posX + parent.posX + width, posY + height + parent.posY);
		GL11.glTexCoord2f(c4.getX(), c4.getY());
		GL11.glVertex2f(posX + parent.posX, posY + height + parent.posY);
		GL11.glEnd();
		GL11.glPopMatrix();
	}

	public Button setPosition(int x, int y) {
		this.posX = x;
		this.posY = y;
		return this;
	}

	public Button setSize(int width, int height) {
		this.width = width;
		this.height = height;
		return this;
	}

	protected Button setOffset(int x, int y) {
		this.texOffX = x;
		this.texOffY = y;
		return this;
	}
	
	@Override
	public boolean isVisible() {
		return super.isVisible() && parent.isVisible();
	}
	
	protected Button setYOffset(int y) {
		this.texOffY = y;
		return this;
	}
	
	protected Button setXOffset(int x) {
		this.texOffX = x;
		return this;
	}

	/**
	 * Set button size and position relative to its parent GUI.
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param offX
	 * @param offY
	 */
	public Button setSizeAndPosition(int x, int y, int width, int height, int offX, int offY) {
		this.setPosition(x, y);
		this.setSize(width, height);
		this.setOffset(offX, offY);
		this.updateTextureCorners();
		return this;
	}

	/**
	 * Returns if point on monitor is in bounds of the button!
	 * 
	 * @param e
	 * @return
	 */
	@Override
	public boolean inBounds(double d, double e, boolean isTranslated) {
		if (!parent.isVisible() && this.isVisible()) return false;
		if ((posX + parent.posX <= d) && (posY + parent.posY <= e)) // Left
			// lower?
			if ((posX + parent.posX + width >= d) && (posY + height + parent.posY >= e))
				return true;
		return false;
	}

	public Texture getTexture() {
		return texture;
	}

	public Button setTexture(Texture texture) {
		this.texture = texture;
		return this;
	}

	/**
	 * Update texture corner positions to allow texture off-setting.
	 */
	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY + height);
		c2 = TextureTools.getTextureCorner(this.texture, texOffX + width, texOffY + height);
		c3 = TextureTools.getTextureCorner(this.texture, texOffX + width, texOffY);
		c4 = TextureTools.getTextureCorner(this.texture, texOffX, texOffY);
	}

	@Override
	public void onDisplayResized(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finalize() {

	}

	@Override
	public boolean isDeleted() {
		return this.delete;
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		this.parent.buttonPressed(this);
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		this.parent.buttonReleased(this);
	}

	@Override
	public void mouseMoved(double x, double y) {
		
	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {
		// TODO Auto-generated method stub
		
	}
}
