package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

public abstract class GameLogicGui extends Gui {
	
	boolean running = false;
	
	public abstract void resume();
	
	public abstract void stop();
	
	public abstract void reset();
	
	public boolean isPaused() {
		return !running;
	}
	public boolean isRunning() {
		return running;
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
}
