package eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.KeyboardController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.MouseController;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IKeyEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;

/**
 * Object that can be updated, but will not be drawn on screen Used to create
 * handlers for DrawableGameObjects (to collect and manage them) or anything
 * else that needs to be calculated every tick, or once in a while.
 * 
 * @since 995995e
 */
public abstract class GameObject {
	
	public static final int PRIORITY_LOWEST = 0x00000000;
	public static final int PRIORITY_VERY_LOW = 0x1fffffff;
	public static final int PRIORITY_LOW = 0x2fffffff;
	public static final int PRIORITY_MEDIUM = 0x3fffffff;
	public static final int PRIORITY_HIGH = 0x5fffffff;
	public static final int PRIORITY_VERY_HIGH = 0x6fffffff;
	public static final int PRIORITY_HIGHEST = 0x7fffffff;

	/**
	 * Set if object will be updated
	 */
	private boolean enabled = true;

	/**
	 * Set if object will be deleted on next update. Do not edit this boolean
	 * unless you do not want to save the data. Use delete() method instead to
	 * also call finalize()
	 */
	public boolean delete = false;

	/**
	 * Set priority in order to define if it must be updated before or after
	 * something. Priority 0 is lowest possible. Will be drawn first, more
	 * important that will stay on top of screen will be drawn later!
	 */
	public int priority = 0;

	/**
	 * Update object parameters, handle something, etc. DO NOT RENDER
	 * ANYTHING!!!
	 */
	public abstract void update(float delta);
	
	/**
	 * Function called to prepare object to be deleted. Save information to
	 * file, log anything if needed.
	 */
	public abstract void finalize();

	public void unregister() {
		this.delete = true;
		finalize();
		ThePhilosophersClient.listOfDeletedGameObjects.add(this);
	}

	/**
	 * register Object to render/update loop with certain priority. Priority 0
	 * will be drawn first, but will stay under other layers!
	 */
	public void register(int priority) {
		this.priority = priority;
		int index = 0;
		boolean added = false;
		if (ThePhilosophersClient.listOfGameObjects.isEmpty()) {
			ThePhilosophersClient.listOfGameObjects.add(0, this);
			added = true;
		} else {
			for (GameObject dgo : ThePhilosophersClient.listOfGameObjects) {
				if (dgo.priority >= priority) {
					index = ThePhilosophersClient.listOfGameObjects.indexOf(dgo);
					ThePhilosophersClient.listOfGameObjects.add(index, this);
					added = true;
					break;
				}
			}
			if (!added) {
				ThePhilosophersClient.listOfGameObjects.add(index + 1, this);
			}
		}
		if (this instanceof IMouseEvent) {
			MouseController.instance().registerMouseEventListener(this, priority);
		}
		if (this instanceof IKeyEvent) {
			KeyboardController.instance().registerKeyboardEventListener(this, priority);
		}
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}