package eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui;

import java.awt.Color;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IKeyEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.TextureTools;

public class GuiTextBox extends DrawableGameObject implements IMouseEvent, IKeyEvent {
	
	protected Gui parent;
	protected TrueTypeFont font;
	protected Color fontcolor = Color.WHITE;
	protected Texture background;
	
	protected String text = "";
	protected String displayString = "";
	protected String defaultString = "";
	protected String cursor = "|";
	protected String fontName = "resources/fonts/DroidSans.ttf";
	private   String pattern = "^[0-9a-zA-Z -,.#@?��������]*$";
	
	protected boolean limitedLenght = false;
	private   boolean cursorEnabled = true;
	private   boolean isHoldingBackspace = false;
	
	protected float cursorSpeed = 1.2f;
	protected float padding = 3f;
	private   float tick = 0;
	private   float deleteThreshold = 0;
	
	protected Vector2f 
			c1 = new Vector2f(0, 0),
			c2 = new Vector2f(0, 0),
			c3 = new Vector2f(0, 0), 
			c4 = new Vector2f(0, 0);

	protected int posX, posY, width, height;
	protected int maxStringLenght = -1;
	protected int fontSize = 20;
	private   int texOffX = 0;
	private   int texOffY = 0;
	

	public GuiTextBox(Gui parent) {
		this.parent = parent;
		parent.listOfGuiTextBoxes.add(this);
		register(parent.priority + 1);
		this.fontSize = 24;
		font = new TrueTypeFont(fontName, fontSize);
	}

	public GuiTextBox setText(String str) {
		this.text = str;
		this.updateDisplayString();
		return this;
	}

	public GuiTextBox setDefaultString(String str) {
		this.defaultString = str;
		this.updateDisplayString();
		return this;
	}

	public void setFocused() {
		ThePhilosophersClient.focused = this;
		this.updateDisplayString();
	}

	public GuiTextBox setFontSize(int size) {
		this.fontSize = size;
		font = new TrueTypeFont(fontName, size);
		return this;
	}

	public GuiTextBox setFontColor(Color c) {
		this.fontcolor = c;
		return this;
	}

	public GuiTextBox setFontName(String name) {
		this.fontName = name;
		font = new TrueTypeFont(name, this.fontSize);
		return this;
	}

	public GuiTextBox setBackground(Texture bg) {
		background = bg;
		return this;
	}

	public GuiTextBox setCursorEnabled(boolean enabled) {
		this.cursorEnabled = enabled;
		return this;
	}

	public GuiTextBox setRegex(String regex) {
		this.pattern = regex;
		return this;
	}

	public GuiTextBox setPositionAndSize(int x, int y, int width, int height) {
		this.width = width;
		this.height = height;
		this.posX = x;
		this.posY = y;
		updateTextureCorners();
		return this;
	}
	
	public GuiTextBox setSize(int width, int height) {
		this.width = width;
		this.height = height;
		updateTextureCorners();
		return this;
	}
	public GuiTextBox setWidth(int width) {
		this.width = width;
		updateTextureCorners();
		return this;
	}

	public GuiTextBox setMaxTextLenght(int lenght, boolean limit) {
		this.limitedLenght = limit;
		this.maxStringLenght = lenght;
		return this;
	}

	public GuiTextBox setCursorSpeed(float speed) {
		this.cursorSpeed = speed;
		return this;
	}

	@Override
	public void update(float delta) {
		if (ThePhilosophersClient.isFocused(this)) {
			tick += delta * cursorSpeed;
			if (tick > 60) {
				tick = 0;
				cursor = cursor.equals("") ? "|" : "";
			}
		}
		if (this.isHoldingBackspace) {
			this.deleteThreshold += delta;
			if (this.deleteThreshold > ThePhilosophersClient.th.tickSpeed) {
				this.deleteThreshold -= ThePhilosophersClient.th.tickSpeed / 30;
				this.removeLastCharacter();
				this.updateDisplayString();
			}
		}
	}

	@Override
	public void draw(float delta) {
		if (!this.isVisible())
			return;
		GL11.glPushMatrix();
		background.bind();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(c1.getX(), c1.getY());
		GL11.glVertex2f(posX + parent.posX, posY + parent.posY);
		GL11.glTexCoord2f(c2.getX(), c2.getY());
		GL11.glVertex2f(posX + parent.posX + width, posY + parent.posY);
		GL11.glTexCoord2f(c3.getX(), c3.getY());
		GL11.glVertex2f(posX + parent.posX + width, posY + height + parent.posY);
		GL11.glTexCoord2f(c4.getX(), c4.getY());
		GL11.glVertex2f(posX + parent.posX, posY + height + parent.posY);
		GL11.glEnd();
		GL11.glPopMatrix();

		background.unbind();
		font.draw(this.displayString + (ThePhilosophersClient.isFocused(this) && isCursorEnabled() ? cursor : ""),
				(int) (this.parent.posX + this.posX + padding),
				(int) (this.parent.posY + this.posY + (this.height / 2 - (this.fontSize / 3f))), this.fontcolor);

	}

	private void addCharacter(String c) {
		if (text.length() < this.maxStringLenght || !this.limitedLenght)
			this.text += c;
	}

	private void removeLastCharacter() {
		if (this.text.length() > 0)
			this.text = this.text.substring(0, this.text.length() - 1);
	}

	public String getText() {
		return this.text;
	}

	public boolean isEmty() {
		if (this.text.length() < 1)
			return true;
		return false;
	}

	public void updateDisplayString() {
		if (text.equals("") && !ThePhilosophersClient.isFocused(this))
			this.displayString = fitStringIntoField(this.defaultString);
		else
			this.displayString = fitStringIntoField(text);
	}

	public boolean isCursorEnabled() {
		return this.cursorEnabled;
	}

	protected String fitStringIntoField(String a) {
		String result = "";
		float length = 0f;
		for (int pos = a.length(); pos > 0; pos--) {
			String c = a.substring(pos - 1, pos);
			length = this.font.getTextWidth(result);
			if (length > this.width - (2 * this.padding) - (isCursorEnabled() ? this.font.getTextWidth("|") : 0)) {
				return result.substring(0, result.length() - 2);
			}
			result = c + result;
		}
		return result;
	}

	@Override
	public boolean inBounds(double x, double y, boolean isTranslated) {
		if ((posX + parent.posX <= x) && (posY + parent.posY <= y))
			if ((posX + parent.posX + width >= x) && (posY + height + parent.posY >= y))
				return true;
		this.updateDisplayString();
		return false;
	}

	@Override
	public void keyReleased(int key, int scancode, int mods) {
		if (ThePhilosophersClient.isFocused(this))
			if (key == GLFW.GLFW_KEY_BACKSPACE) {
				this.deleteThreshold = 0.0f;
				this.isHoldingBackspace = false;
				this.removeLastCharacter();
				this.updateDisplayString();
				return;
			}
		
		if (key == GLFW.GLFW_KEY_ENTER || key == GLFW.GLFW_KEY_KP_ENTER) {
			this.parent.textBoxEntered(this);
			return;
		}
	}

	@Override
	public void keyPressed(int key, int scancode, int mods) {
		if (ThePhilosophersClient.isFocused(this)) {
			if (key == GLFW.GLFW_KEY_BACKSPACE) {
				this.isHoldingBackspace = true;
				return;
			}
		}
	}

	@Override
	public void characterTyped(int codepoint) {
		String k = String.valueOf(((char) codepoint));
		if (k.matches(this.pattern)) {
			this.addCharacter(k);
			this.updateDisplayString();
		}
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		updateDisplayString();
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		ThePhilosophersClient.focused = this;
	}

	@Override
	public void mouseMoved(double x, double y) {

	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {

	}

	@Override
	public void onDisplayResized(int width, int height) {

	}

	@Override
	public void finalize() {
		
	}

	@Override
	public boolean isDeleted() {
		return false;
	}

	@Override
	public boolean isVisible() {
		return super.isVisible() && this.parent.isVisible();
	}

	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.background, texOffX, texOffY + height);
		c2 = TextureTools.getTextureCorner(this.background, texOffX + width, texOffY + height);
		c3 = TextureTools.getTextureCorner(this.background, texOffX + width, texOffY);
		c4 = TextureTools.getTextureCorner(this.background, texOffX, texOffY);
	}
}