package eu.thesociety.DragonbornSR.thephilosophers.client.gui;

import org.lwjgl.opengl.GL11;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.interfaces.IMouseEvent;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.objects.DrawableGameObject;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.ResourceManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.fx.BackgroundParticleSystem;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.Vector2f;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.TextureTools;

public class Background extends DrawableGameObject implements IMouseEvent {

	Texture texture;
	Texture logo;
	
	BackgroundParticleSystem particleSystem;

	int w = Display.WIDTH;
	int h = Display.HEIGHT;

	private Vector2f c1, c2, c3, c4;

	public Background() {
		this.register(PRIORITY_LOWEST);
		texture = ResourceManager.getTexture("background");
		logo = ResourceManager.getTexture("logo");
		this.updateTextureCorners();
		particleSystem = new BackgroundParticleSystem(this.priority + 1);
		
	}

	@Override
	public void update(float delta) {

	}

	@Override
	public void draw(float delta) {

		GL11.glPushMatrix();
		texture.bind();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(c1.getX(), c1.getY());
		GL11.glVertex2f(0, 0);
		GL11.glTexCoord2f(c2.getX(), c2.getY());
		GL11.glVertex2f(w, 0);
		GL11.glTexCoord2f(c3.getX(), c3.getY());
		GL11.glVertex2f(w, h);
		GL11.glTexCoord2f(c4.getX(), c4.getY());
		GL11.glVertex2f(0, h);
		GL11.glEnd();
		texture.unbind();
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		logo.bind();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0.0f, 1f);
		GL11.glVertex2f(w - logo.getWidth(), 0);
		GL11.glTexCoord2f(1f, 1f);
		GL11.glVertex2f(w, 0);
		GL11.glTexCoord2f(1f, 0f);
		GL11.glVertex2f(w, logo.getHeight());
		GL11.glTexCoord2f(0, 0f);
		GL11.glVertex2f(w - logo.getWidth(), logo.getHeight());
		GL11.glEnd();
		logo.unbind();
		GL11.glPopMatrix();
	}

	@Override
	public void onDisplayResized(int width, int height) {
		w = width;
		h = height;
		this.updateTextureCorners();
	}

	@Override
	public void finalize() {
		// TODO Auto-generated method stub
	}

	public void updateTextureCorners() {
		c1 = TextureTools.getTextureCorner(this.texture, 0, h);
		c2 = TextureTools.getTextureCorner(this.texture, w, h);
		c3 = TextureTools.getTextureCorner(this.texture, w, 0);
		c4 = TextureTools.getTextureCorner(this.texture, 0, 0);
	}

	@Override
	public boolean inBounds(double x, double y, boolean isTranslated) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isDeleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void mousePressed(double x, double y, int button, int mods) {
		this.particleSystem.spawnRandomParticleAtPoint((int)x, (int)y);
	}

	@Override
	public void mouseReleased(double x, double y, int button, int mods) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseScrolled(double xDirection, double yDirection) {
		// TODO Auto-generated method stub
		
	}
}
