package eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements;

import eu.thesociety.DragonbornSR.thephilosophers.client.engine.Display;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.resources.Texture;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.Gui;
import eu.thesociety.DragonbornSR.thephilosophers.client.engine.ui.advanced.ScrollableList;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Lobby;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.Matchmaking;

public class ScrollableListServerSelection extends ScrollableList {
	
	public static ScrollableListServerSelection instance;

	public ScrollableListServerSelection(Gui parentGui) {
		super(parentGui, 100, 200, Display.WIDTH - 200,Display.HEIGHT - 300, new Texture("resources/white.png"));
		instance = this;
	}

	@Override
	public void onDisplayResized(int width, int height) {
		super.onDisplayResized(width, height);
		this.width = width - 200;
		this.height = height - 300;
		if (!this.items.isEmpty()) {
			int error = this.height % this.items.get(0).getHeight();
			this.height -= error;
		}
	}
	
	public void updateList() {
		this.items.clear();
		for (Lobby hg : Matchmaking.lobbies) {
			this.items.add(new ScrollableListItemServerSelectionServer(this, hg));
		}
		if (!this.items.isEmpty()) {
			int error = this.height % this.items.get(0).getHeight();
			this.height -= error;
		}
	}
}
