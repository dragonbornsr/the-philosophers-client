package eu.thesociety.DragonbornSR.thephilosophers.client.game;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.codedisaster.steamworks.SteamException;
import com.codedisaster.steamworks.SteamFriends;
import com.codedisaster.steamworks.SteamFriends.PersonaChange;
import com.codedisaster.steamworks.SteamFriendsCallback;
import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamMatchmaking;
import com.codedisaster.steamworks.SteamMatchmaking.ChatMemberStateChange;
import com.codedisaster.steamworks.SteamMatchmaking.ChatRoomEnterResponse;
import com.codedisaster.steamworks.SteamMatchmaking.LobbyType;
import com.codedisaster.steamworks.SteamMatchmakingCallback;
import com.codedisaster.steamworks.SteamResult;

import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager;
import eu.thesociety.DragonbornSR.thephilosophers.client.ThePhilosophersClient;
import eu.thesociety.DragonbornSR.thephilosophers.client.game.mission.Mission;
import eu.thesociety.DragonbornSR.thephilosophers.client.GameStateManager.GameState;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListLobbyChat;
import eu.thesociety.DragonbornSR.thephilosophers.client.gui.elements.ScrollableListServerSelection;
import eu.thesociety.DragonbornSR.thephilosophers.client.types.ChatMessage;
import eu.thesociety.DragonbornSR.thephilosophers.client.utils.Crypto;

/**
 * Class to handle games on the internet that have not been started, yet.
 * @author Oliver
 *
 */
public class Matchmaking implements SteamMatchmakingCallback, SteamFriendsCallback {
	private static Matchmaking instance;
	private static SteamMatchmaking matchmaking;
	private static SteamFriends steamFriends;
	
	public static ArrayList<Lobby> lobbies = new ArrayList<Lobby>();
	
	private final ByteBuffer chatMessage = ByteBuffer.allocateDirect(4096);
	private final SteamMatchmaking.ChatEntry chatEntry = new SteamMatchmaking.ChatEntry();
	private final Charset messageCharset = Charset.forName("UTF-8");
	private static Lobby currentLobby;
	
	Mission mission;
	
	private Matchmaking() {
		matchmaking = new SteamMatchmaking(this);
		steamFriends = new SteamFriends(this);
	}
	
	public static Matchmaking instance() {
		if (instance == null)
			instance = new Matchmaking();
		return instance;
	}

	public void makeGame(Mission mission) {
		this.mission = mission;
		if (mission != null) {
			matchmaking.createLobby(LobbyType.Public, mission.getMissionPlayers().size());
			System.out.println("creating mission");
		} else {
			System.out.println("Mission Creation Failed");
		}
	}

	@Override
	public void onLobbyCreated(SteamResult result, SteamID steamIDLobby) {
		if (result == SteamResult.OK) {
			matchmaking.setLobbyData(steamIDLobby, Lobby.KEY_GAMEKEY, Crypto.MD5(ThePhilosophersClient.gamekey));
			matchmaking.setLobbyData(steamIDLobby, Lobby.KEY_HOST_ID, "?");
			matchmaking.setLobbyData(steamIDLobby, Lobby.KEY_HOST_NAME, "?");
			matchmaking.setLobbyData(steamIDLobby, Lobby.KEY_MAX_MEMBERS, ""+mission.getMissionPlayers().size());
			matchmaking.setLobbyData(steamIDLobby, Lobby.KEY_MISSION_LOCATION, mission.getMissionLocation());
			GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY);
			ScrollableListLobbyChat.instance.clear();
			currentLobby = new Lobby(steamIDLobby);
		} else {
			
		}
	}
	
	@Override
	public void onFavoritesListChanged(int ip, int queryPort, int connPort, int appID, int flags, boolean add,
			int accountID) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void onLobbyInvite(SteamID steamIDUser, SteamID steamIDLobby, long gameID) {
		System.out.println("you were invited");
	}

	@Override
	public void onLobbyEnter(SteamID steamIDLobby, int chatPermissions, boolean blocked,
			ChatRoomEnterResponse response) {
		Lobby.updateLobbyData(steamIDLobby);
		ScrollableListLobbyChat.instance.clear();
		currentLobby = new Lobby(steamIDLobby);
		GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY);
		
	}

	@Override
	public void onLobbyDataUpdate(SteamID steamIDLobby, SteamID steamIDMember, boolean success) {
		Lobby.updateLobbyData(steamIDLobby);
		System.out.println("data update");
	}

	@Override
	public void onLobbyChatUpdate(SteamID steamIDLobby, SteamID steamIDUserChanged, SteamID steamIDMakingChange,
			ChatMemberStateChange stateChange) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLobbyChatMessage(SteamID steamIDLobby, SteamID steamIDUser, SteamMatchmaking.ChatEntryType entryType, int chatID) {
		try {
			int size = matchmaking.getLobbyChatEntry(steamIDLobby, chatID, chatEntry, chatMessage);
			byte[] bytes = new byte[size];
			chatMessage.get(bytes);
			String message = new String(bytes, messageCharset);
			if (currentLobby.getPlayer(steamIDUser).isLoaded())
				ScrollableListLobbyChat.instance.addMessage(new ChatMessage(chatEntry.getSteamIDUser(), 
						message.substring(0, message.length() - 1), 
						chatEntry.getChatEntryType()));
			chatMessage.clear();

		} catch (SteamException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onLobbyGameCreated(SteamID steamIDLobby, SteamID steamIDGameServer, int ip, short port) {
		ScrollableListLobbyChat.instance.clear();
	}

	@Override
	public void onLobbyMatchList(int lobbiesMatching) {
		System.out.println("Found " + lobbiesMatching + " lobbies");
		for (int i = 0; i < lobbiesMatching; i++) {
			SteamID lobby = matchmaking.getLobbyByIndex(i);
			String lobbyId = lobby.toString();
			if (lobbies.stream().anyMatch(l -> l.getSteamID().toString().equals(lobbyId))) continue;
			Lobby hg = new Lobby(lobby);
			hg.updateLobbyData();
			if (hg.isValid()) {
				lobbies.add(hg);
			}
		}
		ScrollableListServerSelection.instance.updateList();
	}

	@Override
	public void onLobbyKicked(SteamID steamIDLobby, SteamID steamIDAdmin, boolean kickedDueToDisconnect) {
		GameStateManager.instance().setCurrentGameState(GameState.MP_LOBBY_SELECTION);
		ScrollableListLobbyChat.instance.clear();
		if (Matchmaking.currentLobby != null) Matchmaking.currentLobby.updateLobbyUserlist();
		currentLobby = null;
	}

	@Override
	public void onFavoritesListAccountsUpdated(SteamResult result) {
		// TODO Auto-generated method stub
	}

	/**
	 * @return the lobbyId
	 */
	public static SteamID getLobbyId() {
		return currentLobby.getSteamID();
	}
	
	public SteamMatchmaking getSteamMatchmaking() {
		return matchmaking;
	}
	
	/**
	 * @return the steamFriends
	 */
	public SteamFriends getSteamFriends() {
		return steamFriends;
	}

	/**
	 * @return the currentLobby
	 */
	public static Lobby getCurrentLobby() {
		return currentLobby;
	}

	/**
	 * @param currentLobby the currentLobby to set
	 */
	public static void setCurrentLobby(Lobby currentLobby) {
		Matchmaking.currentLobby = currentLobby;
	}

	@Override
	public void onPersonaStateChange(SteamID steamID, PersonaChange change) {
		
	}

	@Override
	public void onGameOverlayActivated(boolean active) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGameLobbyJoinRequested(SteamID steamIDLobby, SteamID steamIDFriend) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAvatarImageLoaded(SteamID steamID, int image, int width, int height) {
		// TODO Auto-generated method stub
		
	}
}